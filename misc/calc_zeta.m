%quick script to calculate zeta_aa from layer boundaries

za = [0, 0.1393, 0.2987, 0.4665, 0.6402, 0.8183, 1.0000];
zeta_aa = zeros(numel(za)+1, 1);
zeta_aa(1) = za(1); zeta_aa(end) = za(end);


for ii = 2:(numel(zeta_aa)-1)
    
    zeta_aa(ii) = (za(ii) + za(ii-1))/2;

end

zeta_aa











