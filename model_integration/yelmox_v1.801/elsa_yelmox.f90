!This subrountine takes yelmox input data,
!converts it into a form readable by the ELSA subroutines, passes it
!through the ELSA subroutines, and then constructs an output.

!Based on the elsa0.f90

!therese.rieckh@uib.no, MELT team, University of Bergen, 2023

module elsay

 !load modules
 use elsa_classes
 use f90getopt
 use netcdf
 use m_npy
 use nml
 use elsa_isochrones
 use load_input_data


 implicit none

contains

 subroutine initialize_elsa(elsa1,nml_in,time_init,time_end,time,dtt,imax,jmax,dx,dy,h_ice)

  implicit none

  type(elsa_class), intent(inout)      :: elsa1 !elsa class. For initialisation this can be empty
  integer, intent(in)                  :: imax, jmax !size of incoming file/binary from model
  real(4), intent(in)                  :: dx, dy !x spacing, y spacing
  real(4), intent(in)                  :: dtt !model time step
  real(4), intent(in)                  :: time_init, time_end, time
  real(4), dimension(imax,jmax), intent(in)   :: h_ice !ice thickness
  character(len=512), intent(inout)    :: nml_in !parameter file location
  character(len=512)                   :: layer_file, source_path, command
  integer :: status
  logical :: layer_file_exists

  call read_arguments(nml_in)
  call nml_read(nml_in,"elsa","layer_file",layer_file)

  if (layer_file .ne. 'NA') then
     ! Hack to copy layer_file to output/run directory, where yelmox_elsa.x is executed
     source_path = '../../par/' // layer_file
     inquire(file=source_path, exist=layer_file_exists)

     if ( layer_file_exists ) then
        command = 'cp ' // trim(source_path) // ' ' // trim(layer_file)
        ! Execute the command to copy the file
        status = system(command)
     else
        write(*,*) "initialize_elsa:: Error: layer_file does not exist"
        stop
     end if
  end if

  call elsa_init(elsa1,nml_in,dble(time_init),dble(time_end),dble(dtt),imax,jmax,dble(dx),dble(dy),dble(H_ice), layer_file)
  write(*,"(a,i5,a,f10.2)") 'elsa_add_layer   : add new layer     | top_layer: ',elsa1%now%top_layer,' | time: ',time

 end subroutine initialize_elsa


 subroutine update_elsa(elsa1,model1,time)

  implicit none

  type(elsa_class), intent(inout)      :: elsa1
  type(model_class), intent(inout)     :: model1
  real(4), intent(in)                  :: time

  ! Local variables
  integer                :: ii


  call elsa_add_smb(elsa1,elsa1%par%update_dt,model1%grd%par%nx,model1%grd%par%ny,model1%mb%now%smb)
  call elsa_add_bmb(elsa1,elsa1%par%update_dt,model1%grd%par%nx,model1%grd%par%ny,model1%mb%now%bmb)
  call elsa_update(model1,elsa1,elsa1%par%update_dt)


  if ( any(elsa1%par%layer_list == time) ) then
     call elsa_add_layer(elsa1)
     write(*,"(a,i5,a,f10.2)") 'elsa_add_layer   : add new layer     | top_layer: ',elsa1%now%top_layer,' | time: ',time
  end if


 end subroutine update_elsa


 subroutine write_elsa_npy(exp_name,run_year,elsa1)

  implicit none

  type(elsa_class), intent(inout)        :: elsa1
  character(len=40), intent(in)          :: exp_name
  integer, intent(in)                    :: run_year
  character(len=20)                      :: varname
  character(len=120)                     :: filename
  character(len=256)                     :: spec_format

  varname = 'diso'
  write (spec_format, '("(A", I0,",A1,I6.6,A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",run_year,".",varname,".npy"
  call save_npy(filename, elsa1%now%d_iso(:,:,:))

  varname = 'dsum'
  write (spec_format, '("(A", I0,",A1,I6.6,A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",run_year,".",varname,".npy"
  call save_npy(filename, elsa1%now%dsum_iso(:,:,:))

  varname = 'hice'
  write (spec_format, '("(A", I0,",A1,I6.6,A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",run_year,".",varname,".npy"
  call save_npy(filename, elsa1%now%H_ice_iso(:,:))

  varname = 'vx'
  write (spec_format, '("(A", I0,",A1,I6.6,A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",run_year,".",varname,".npy"
  call save_npy(filename, elsa1%now%vx_iso(:,:,:))

  varname = 'vy'
  write (spec_format, '("(A", I0,",A1,I6.6,A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",run_year,".",varname,".npy"
  call save_npy(filename, elsa1%now%vy_iso(:,:,:))

 end subroutine write_elsa_npy


 subroutine write_elsa_final(exp_name,elsa1)

  implicit none

  type(elsa_class), intent(inout)        :: elsa1
  character(len=40), intent(in)          :: exp_name
  character(len=20)                      :: varname
  character(len=120)                     :: filename
  character(len=256)                     :: spec_format

  varname = 'diso'
  write (spec_format, '("(A", I0,",A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",varname,".npy"
  call save_npy(filename, elsa1%now%d_iso(:,:,:))

  varname = 'dsum'
  write (spec_format, '("(A", I0,",A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",varname,".npy"
  call save_npy(filename, elsa1%now%dsum_iso(:,:,:))

  varname = 'hice'
  write (spec_format, '("(A", I0,",A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",varname,".npy"
  call save_npy(filename, elsa1%now%H_ice_iso(:,:))

  varname = 'vx'
  write (spec_format, '("(A", I0,",A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",varname,".npy"
  call save_npy(filename, elsa1%now%vx_iso(:,:,:))

  varname = 'vy'
  write (spec_format, '("(A", I0,",A1,A", I2,",A4)")') len_trim(exp_name), len_trim(varname)
  write (filename, spec_format) exp_name,".",varname,".npy"
  call save_npy(filename, elsa1%now%vy_iso(:,:,:))

 end subroutine write_elsa_final


 subroutine write_elsa_bin(exp_name,run_year,elsa1)

  implicit none

  type(elsa_class), intent(in)         :: elsa1
  character(len=256), intent(in)       :: exp_name
  integer, intent(in)                  :: run_year

  call write_bin(exp_name,run_year,sngl(elsa1%now%H_ice_iso), &
     elsa1%par%nx_iso,elsa1%par%ny_iso,1,'h_ice_iso')
  call write_bin(exp_name,run_year,sngl(elsa1%now%dsum_iso), &
     elsa1%par%nx_iso,elsa1%par%ny_iso,elsa1%par%n_layers,'dsum_iso')
  call write_bin(exp_name,run_year,sngl(elsa1%now%d_iso), &
     elsa1%par%nx_iso,elsa1%par%ny_iso,elsa1%par%n_layers,'d_iso')
  call write_bin(exp_name,run_year,sngl(elsa1%now%vx_iso), &
     elsa1%par%nx_iso,elsa1%par%ny_iso,elsa1%par%n_layers,'vx_iso')
  call write_bin(exp_name,run_year,sngl(elsa1%now%vy_iso), &
     elsa1%par%nx_iso,elsa1%par%ny_iso,elsa1%par%n_layers,'vy_iso')

  if (elsa1%par%tracer_on) then
     call write_bin(exp_name,run_year,sngl(elsa1%now%tracer_iso), &
        elsa1%par%nx_iso,elsa1%par%ny_iso,elsa1%par%n_layers,'tracer_iso')
  end if

 end subroutine write_elsa_bin


 subroutine write_bin(exp_name,run_year,variable,nx,ny,nz,var_file_name)

  implicit none

  character(len=*), intent(in)         :: exp_name
  character(len=*), intent(in)         :: var_file_name
  character(len=120)                   :: filename
  character(len=256)                   :: spec_format
  integer                              :: run_year, nx, ny, nz, ierr
  real, dimension(nx,ny,nz)            :: variable

  write (spec_format, '("(A", I0,",A1,I6.6,A1,A", I2,",A4)")') len_trim(exp_name), len_trim(var_file_name)
  write (filename, spec_format) exp_name,".",run_year,".",var_file_name,".bin"
  open(unit=10,iostat=ierr,status='replace',file=filename,form='unformatted',access='stream')
  write(10) variable
  close(10)

  return
 end subroutine write_bin


end module elsay
