#ELSA-0 compilation script
#MELT team
#University of Bergen, 2023

echo "compile script starts, this is really just a minimal working example for including a homemade library in a compile"
cd src

FC=gfortran #the compiler to use, only tested for gfortran

NCF=-I/usr/include #the location of netcdf.mod with the '-I' flag for 'include'. This can be installed with 'apt-get install libnetcdff-dev'. 
libNCF="-lnetcdff -lnetcdf"
#NETCDF_DIR still needs to point to the correct path (e.g. export NETCDF_DIR=/lib/x86_64-linux-gnu/libnetcdf.so in your .bashrc)

lis=-I/work2/trieckh/libs/lis/include #location of the lis library header lisf.h. Follow the docs carefully and make sure to use --enable-f90 and --enable-fortran flags.
liblis="-L/work2/trieckh/libs/lis/lib -llis"

echo "compiling objects (.o files)"
$FC -cpp -g \
        elsa_classes.f90 npy.f90 nml.f90 load_netcdf.f90 f90getopt.F90 elsa_isochrones.f90 \
	-c $NCF $lis $liblis $libNCF #-g flag can be included for debugging but is not essential

echo "objects compiled, creating library"
ar rc libelsa_bits.a elsa_classes.o npy.o nml.o load_netcdf.o f90getopt.o elsa_isochrones.o

echo "libelsa_bits.a is ready to go, using this to compile elsa0 from a library"

libelsa_bits="-L/work2/rob/git-repos/elsa-0/src -lelsa_bits"

$FC -cpp -g sandbox.f90 -o elsa0 $libelsa_bits $NCF $lis $liblis $libNCF

if [ -f ./elsa0 ]; then
  echo "elsa0 compiled, tidying"
else
  echo " " #something went wrong but you probably have an error for that already
fi

echo "moving elsa0 out of src, removing .mod, .o, and .a files to tidy -- though you may wish to retain the .a library file"
mv elsa0 ../elsa0
rm *.mod
rm *.o
rm *.a
cd ../

