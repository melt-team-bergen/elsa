"""
Description: basic figures created from ELSA output

Author: Therese Rieckh | therese.rieckh@uib.no
"""

import argparse
import numpy as np
import scipy as sc
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rcParams.update({'font.size': 30})
import matplotlib.ticker as mticker

import copy
import decimal
import os
import os.path as op
import re
import sys
from importlib import reload


from scipy.interpolate import RegularGridInterpolator

import plot_utilities as pu

# sys.path.insert(0,'/Home/siv33/tri061/git-repos/utils/colorblind_scheme/')
sys.path.insert(0,'/home/rese/git-work/utils/')
import colorblind
# sys.path.insert(0,'/Home/siv33/tri061/git-repos/elsa-analysis-library/')
# import array_tool as at # NOTE REMOVE

from mpl_toolkits.axes_grid1 import make_axes_locatable
from netCDF4 import Dataset


EXP = 'TODO'
# EXP = 'GMD-001-03-il_advect-0p1_dble_variable-cutoff/GRL16_PMIP-2-pal_0001'

PLOT_BASE = '/tmp/plots/'
BASE = '/Home/siv33/tri061/git-repos/elsa-yelmo-coupled/'
NASA = '/Home/siv33/tri061/nasa/'
# BASE = '/home/rese/git-work/'
# NASA = '/home/rese/nasa/'

YELMOX_OUTPUT = 'yelmox/output/'
YELMOX = 'yelmox/'
ELSA_OUTPUT = 'elsa/output/'


PLOT = op.join(PLOT_BASE, EXP)
YELMOX_IN = op.join(YELMOX_OUTPUT, EXP)
ELSA_IN = op.join(YELMOX_OUTPUT, EXP)


def main():
    """
    plots a few basic figures to evaluate a simulation
    - DONE cross sections at 4 latitudes
    - DONE layer depth difference of modeled and reconstructed isochrones
    - DONE layer depth difference normalized with reconstructed isochrone uncertainty
    - DONE ice thickness at various points in time (default: PD, LGM, Eemian)
    - DONE present-day bed properties
    - DONE difference (wrt BedMachine) of present-day ice thickness and present-day ice velocity
    - TODO time series of variables over the course of the simulation
    """
    if not op.isdir(PLOT):
        os.makedirs(PLOT)

    lats = [66, 71, 76, 79]
    end_of_run = 2000

    nmly = '{}.nml'.format(op.basename(EXP))
    runy = pu.ElsaYelmo(op.join(BASE, YELMOX_IN), nmly)

    ## USED WITH SPECIFIC SET AGES
    # ages = np.array([-10000, -27000, -55000, -113000])
    # plot_crosssection(runy, end_of_run, lats, ages_used=ages)
    # plot_map_isodepth_difference(runy, end_of_run, abs_=True, ages_used=ages)
    # plot_map_isodepth_difference(runy, end_of_run, abs_=False, ages_used=ages)

    ## USED WITH OIB *V1* AND ALL AVAILABLE OIB LAYERS
    # plot_crosssection(runy, end_of_run, lats, version='v1')
    # plot_map_isodepth_diff_between_layers(runy, end_of_run, version='v1')
    # plot_map_isodepth_difference(runy, end_of_run, abs_=True, version='v1')
    # plot_map_isodepth_difference(runy, end_of_run, abs_=False, version='v1')

    ## USED WITH OIB *V2* AND ALL AVAILABLE OIB LAYERS
    plot_crosssection(runy, end_of_run, lats)
    plot_map_isodepth_diff_between_layers(runy, end_of_run)
    plot_map_isodepth_difference(runy, end_of_run, abs_=True)
    plot_map_isodepth_difference(runy, end_of_run, abs_=False)

    ## MODEL OUTPUT PLOTS
    plot_map_hice(runy, [-122000, 2000], contour_pd=True)
    plot_map_hice(runy, [-22000], contour_contshelf=True)
    plot_map_bed_properties(runy, end_of_run)
    plot_map_pd_diff(runy, 'hice')
    plot_map_pd_diff(runy, 'uxy')


def plot_crosssection(run_, time, lats, ages_used=np.array([]), version='v2'):
    """
    Plots ice sheet cross sections at given latitudes, including all isochrone information from ELSA.
    Reconstructed isochrones are also shown (dashed lines).
    Modeled isochrones of same age as reconstructed ones are colored accordingly.

    run_: ELSA() object
    time: time for cross section (CE)
    lats: list of latitudes for cross sections
    """
    lats_idcs = [run_.idx_latlon((x, -30))[0] for x in lats]
    zbed, idx = run_.get_bed_from_netcdf(time)
    coastline = zbed[idx]
    mask_run = run_.get_ice_mask()

    nasa = pu.OIBObs(16, op.join(BASE, YELMOX), NASA, version=version)
    if ages_used.size == 0:
        age_idcs = np.in1d(nasa.age_iso_ce, nasa.age_iso_ce).nonzero()[0]
        ages_used = nasa.age_iso_ce
    else:
        age_idcs = np.in1d(nasa.age_iso_ce, ages_used).nonzero()[0]

    colors = colorblind.qualitative_colors(len(age_idcs)+1)
    dbsdata = np.array([run_.get_isochrone_depth_below_surface(isochrone) for isochrone in nasa.age_iso_ce[age_idcs]])
    isodata = run_.load_npy('dsum')

    for jj, ii in enumerate(lats_idcs):
        fig, ax = plt.subplots(figsize=(12,6))
        bed = run_.load_nc('z_bed')[-1][ii]
        hice_run = run_.load_npy('hice')[ii]
        hice_obs = nasa.thickness[ii]

        plt.plot(hice_run+bed, color=colors[0], label='2ka CE', lw=2)
        plt.plot(hice_obs+bed, color=colors[0], ls='--', lw=2)
        for idx in np.arange(0, isodata.shape[0]):
            plt.plot(isodata[idx,ii,:]+bed, color='grey', lw=0.3)
        for x, idx in enumerate(age_idcs):
            rmse = compute_rmse(dbsdata[x][ii], nasa.depth_iso[idx][ii])
            plt.plot(hice_run-dbsdata[x][ii]+bed, color=colors[x+1], label='{0}ka CE, RMSE: {1}'.format(np.round(nasa.age_iso_ce[idx]/1000., 1), rmse), lw=2)
            # plt.plot(hice_run-dbsdata[x][ii]+bed, color=colors[x+1], label='{0}ka CE, RMSE: {1}'.format(np.round(ages_used[idx]/1000., 1), rmse), lw=2)
            # if idx == 0:
            #     plt.plot(hice_run-dbsdata[idx][ii]+bed, color=colors[idx+1], label='{0}ka CE, RMSE: {1}'.format(iso_ages[idx]/1000., rmse), lw=2)
            # else:
            #     plt.plot(hice_run-dbsdata[idx][ii]+bed, color=colors[idx+1], label='{0}ka CE, RMSE: {1}'.format(decimal.Decimal(iso_ages[idx]/1000.).normalize(), rmse), lw=2)
            plt.plot(hice_obs-nasa.depth_iso[idx][ii]+bed, color=colors[x+1], ls='--', lw=2)

        plt.plot(bed, color='k', lw=2)
        plt.legend(loc='upper left', prop={'size':10})
        plt.text(85, 3000, '$-$ modeled')
        plt.text(85, 2700, '-- reconstructed')

        plt.xticks([])
        plt.ylim(-500,3250)
        plt.yticks([-500,0,500,1000,1500,2000,2500,3000])
        plt.ylabel('Elevation (m)')
        plt.title('Cross section at {}$^\circ$N; RMSE of depth below surface (m)'.format(lats[jj]))

        fnout = op.join(PLOT, 'cross-section_isochrone-depth_{}degN.png'.format(lats[jj]))
        plt.savefig(fnout, bbox_inches='tight', dpi=300)
        plt.close()
        print('Cross section plot completed')


def plot_map_isodepth_diff_between_layers(run_, time, version='v2'):
    """
    Plots a map of isochrone depth difference between neighoring reconstructed isochrones.

    run_: ELSA() object
    time: time to plot comparison (CE), usually present-day
    """
    zbed, idx = run_.get_bed_from_netcdf(time)
    coastline = zbed[idx]

    nasa = pu.OIBObs(16, op.join(BASE, YELMOX), NASA, version=version)

    sett = plotsett('map_obs-layer-diff')
    fnout = 'map_obs-layer-diff_'

    for ii, age in enumerate(nasa.age_iso_ce[:-1]):
        diff = (nasa.depth_iso[ii] - nasa.depth_iso[ii+1])
        print(ii, age)
        fig, ax = plt.subplots(figsize=(8.3,12))
        divider = make_axes_locatable(ax)
        cm = plt.get_cmap(sett['cmap'])
        cm.set_bad('darkgrey', alpha=1)

        plt.pcolormesh(diff, cmap=cm, vmin=sett['lim'][0],vmax=sett['lim'][1])
        ax.contour(coastline, levels=[0], colors='black', linewidths=0.4, zorder=1)
        ax.axis('off')
        plt.title(''.join([sett['title'], '\n {} - {} ka CE isochrone'.format(nasa.age_iso_ce[ii]/1000, nasa.age_iso_ce[ii+1]/1000)]))

        cax = divider.append_axes("right", size="5%", pad=0.2)
        cb = plt.colorbar(cax=cax, extend='both')
        for t in cb.ax.get_yticklabels():
            t.set_fontsize(20)
        plt.savefig(op.join(PLOT, ''.join([fnout,str(nasa.age_iso_ce[ii]).zfill(9), 'CE','.png'])), bbox_inches='tight', dpi=300)
        plt.clf()
        plt.close()
        print('Isochrone OIB layer depth difference plot completed')


def plot_map_isodepth_difference(run_, time, abs_=True, ages_used=np.array([]), version='v2'):
    """
    Plots a map of isochrone depth difference between modeled and reconstructed isochrones.
    Default are absolute differences.

    run_: ELSA() object
    time: time to plot comparison (CE), usually present-day
    abs_: if set to False, the difference normalized with uncertainty from reconstructed isochrones is shown
    """
    zbed, idx = run_.get_bed_from_netcdf(time)
    coastline = zbed[idx]

    nasa = pu.OIBObs(16, op.join(BASE, YELMOX), NASA, version=version)
    if ages_used.size == 0:
        age_idcs = np.in1d(nasa.age_iso_ce, nasa.age_iso_ce).nonzero()[0]
        ages_used = nasa.age_iso_ce
    else:
        age_idcs = np.in1d(nasa.age_iso_ce, ages_used).nonzero()[0]
    dbsdata = np.array([run_.get_isochrone_depth_below_surface(isochrone) for isochrone in nasa.age_iso_ce[age_idcs]])

    if abs_ == True:
        sett = plotsett('map_dbsdiff_abs')
        fnout = 'map_isochrone-depth-difference_abs_'
    else:
        sett = plotsett('map_dbsdiff_rel')
        fnout = 'map_isochrone-depth-difference_rel_'
        if version == 'v1':
            obs_uncdata = nasa.depth_iso_uncert
        elif version == 'v2':
            obs_uncdata = nasa.depth_iso_std

    for ii, idx in enumerate(age_idcs):
        if abs_ == True:
            diff = (dbsdata[ii] - nasa.depth_iso[idx])
        else:
            # TODO: rethink this, how to use std (has negative values??) vs uncertainty?
            diff = (dbsdata[ii] - nasa.depth_iso[idx])/np.abs(obs_uncdata[idx])

        fig, ax = plt.subplots(figsize=(8.3,12))
        divider = make_axes_locatable(ax)
        cm = plt.get_cmap(sett['cmap'])
        cm.set_bad('darkgrey', alpha=1)

        plt.pcolormesh(diff, cmap=cm, vmin=sett['lim'][0],vmax=sett['lim'][1])
        ax.contour(coastline, levels=[0], colors='black', linewidths=0.4, zorder=1)
        ax.axis('off')
        plt.title(''.join([sett['title'], '\n {}ka CE isochrone'.format(nasa.age_iso_ce[idx]/1000)]))

        cax = divider.append_axes("right", size="5%", pad=0.2)
        cb = plt.colorbar(cax=cax, extend='both')
        for t in cb.ax.get_yticklabels():
            t.set_fontsize(20)
        plt.savefig(op.join(PLOT, ''.join([fnout,str(nasa.age_iso_ce[idx]).zfill(9), 'CE','.png'])), bbox_inches='tight', dpi=300)
        plt.clf()
        plt.close()
        print('Isochrone depth difference plot completed')


def plot_map_hice(run_, time, contour_pd=False, contour_contshelf=False):
    """
    plots ice thickness of given time (CE).
    contour_pd adds the outline of the present-day ice sheet boundary
    contour_contshelf adds the outline of the continental shelf (700m contour below sea level)
    """
    sett = plotsett('hice')
    hice, info = run_.load_nc('H_ice', get_time=True)
    hice[hice==0] = np.nan
    zbed = run_.get_bed_from_netcdf()

    for it, tt in enumerate(time):
        fig, ax = plt.subplots(figsize=(8.3,12))
        divider = make_axes_locatable(ax)
        cm = plt.get_cmap(sett['cmap'], 7)
        cm.set_bad('darkgrey', alpha=1)

        ii = np.ma.argmin(np.abs(info['time']-tt))
        timediff = np.abs(info['time'][ii]-tt)
        if timediff > 0:
            print('Warning: exact desired time could not be extracted from data, time difference is {}'.format(timediff))

        plt.pcolormesh(hice[ii], cmap=cm, vmin=sett['lim'][0],vmax=sett['lim'][1])
        ax.contour(zbed[ii], levels=[0], colors='black', linewidths=0.4, zorder=1)
        if contour_pd == True:
            nc_m17 = Dataset(op.join(BASE, YELMOX_IN, 'ice_data/Greenland/GRL-16KM/GRL-16KM_TOPO-M17.nc'), 'r')
            hice_pd = nc_m17.variables['H_ice'][:].copy()
            nc_m17.close()
            ax.contour(hice_pd, levels=[0], colors='magenta', linewidths=3, zorder=1)
        if contour_contshelf == True:
            ax.contour(zbed[ii], levels=[-700], colors='black', linewidths=4, linestyles='dashed', zorder=1)
        ax.axis('off')
        ax.set_title('{0}, {1} CE'.format(sett['title'], tt))
        # ax.scatter([x[1][1] for x in cores], [x[1][0] for x in cores], marker='o', color='r', s=10)

        cax = divider.append_axes("right", size="5%", pad=0.2)
        cb = plt.colorbar(cax=cax)
        for t in cb.ax.get_yticklabels():
            t.set_fontsize(20)

        fnout = op.join(PLOT, 'map_hice_{}CE.png'.format(str(tt).zfill(7)))
        plt.savefig(fnout, bbox_inches='tight', dpi=300)
        plt.clf()
        plt.close()
        print('Ice thickness plot completed')


def plot_map_bed_properties(run_, time):
    """
    Plots a map of the bed properties (frozen, thawed, floating ice)

    run_: ELSA() object
    time: time to bed properties (CE)
    """
    bed_mask, info = run_.load_nc('mask_bed', get_time=True)
    ii = np.ma.argmin(np.abs(info['time']-time))
    timediff = np.abs(info['time'][ii]-time)
    if timediff > 0:
        print('Warning: exact desired time could not be extracted from data, time difference is {}'.format(timediff))

    # mask_bed_ocean   = 0
    # mask_bed_land    = 1
    # mask_bed_frozen  = 2
    # mask_bed_stream  = 3
    # mask_bed_grline  = 4
    # mask_bed_float   = 5
    # mask_bed_island  = 6
    # mask_bed_partial = 7

    bed_mask = bed_mask[ii]
    bed = copy.copy(bed_mask)
    bed[bed_mask==2] = 1
    bed[bed_mask==3] = 2
    bed[bed_mask==1] = 3
    bed[bed_mask==7] = 5
    zbed = run_.get_bed_from_netcdf()

    fig, ax = plt.subplots(figsize=(8.3,12))
    divider = make_axes_locatable(ax)
    cm = plt.get_cmap('terrain', 6)

    plt.pcolormesh(bed, cmap=cm, vmin=-0.5, vmax=5.5)
    ax.contour(zbed[ii], levels=[0], colors='black', linewidths=0.4, zorder=1)

    cax = divider.append_axes("right", size="5%", pad=0.2)
    cbar = plt.colorbar(cax=cax, orientation='vertical')
    cbar.set_ticks([0, 1, 2, 3, 4, 5])
    cbar.ax.set_yticklabels(['ocean', 'frozen', 'stream', 'land', 'grnd line', 'float'],
                            rotation=90, verticalalignment='center', fontsize=20)
    ax.axis('off')
    ax.set_title('{0}, {1} CE'.format(plotsett('bed_mask')['title'], time))

    fnout = op.join(PLOT, 'map_bed_mask.png')
    plt.savefig(fnout, bbox_inches='tight', dpi=300)
    plt.clf()
    plt.close()
    print('Bed properties plot completed')


def plot_map_pd_diff(run_, varn):
    """
    Plots a map of variable difference between model output and reference data for present-day.
    Ice thickness: from Morlighem (2017)
    Ice surface velocity: from Joughin (2018)

    run_: ELSA() object
    varn: variable name (hice or uxy_srf
    """
    if varn == 'hice':
        nc_ = Dataset(op.join(BASE, YELMOX_IN, 'ice_data/Greenland/GRL-16KM/GRL-16KM_TOPO-M17.nc'), 'r')
        ref = nc_.variables['H_ice'][:].copy()
        nc_.close()
        var = run_.load_npy('hice')
    elif varn == 'uxy':
        nc_ = Dataset(op.join(BASE, YELMOX_IN, 'ice_data/Greenland/GRL-16KM/GRL-16KM_VEL-J18.nc'), 'r')
        ref = nc_.variables['uxy_srf'][:].copy()
        nc_.close()
        var = run_.load_nc('uxy_s')[-1]
    else:
        errmsg = 'Variable {} not implemented, check variable name or add path info and infos in plotsett()'.format(varn)
        print(errmsg)
        return

    diff = var-ref
    zbed = run_.get_bed_from_netcdf()
    diff[diff==0.0] = np.nan

    varn = varn+'_diff'
    sett = plotsett(varn)
    fig, ax = plt.subplots(figsize=(8.3,12))
    divider = make_axes_locatable(ax)
    cm = plt.get_cmap(sett['cmap'])
    cm.set_bad('darkgrey', alpha=1)

    plt.pcolormesh(diff, cmap=cm, vmin=sett['lim_abs'][0],vmax=sett['lim_abs'][1])
    ax.contour(zbed[-1], levels=[0], colors='black', linewidths=0.4, zorder=1)
    ax.axis('off')
    ax.set_title(sett['title'])

    cax = divider.append_axes("right", size="5%", pad=0.2)
    cb = plt.colorbar(cax=cax, extend='both')
    for t in cb.ax.get_yticklabels():
        t.set_fontsize(20)

    fnout = op.join(PLOT, 'map_diff_{}.png'.format(varn))
    plt.savefig(fnout, bbox_inches='tight', dpi=300)
    plt.clf()
    plt.close()
    print('PD difference plots completed')


def compute_rmse(modeldata, obsdata):
    """
    """
    diff = modeldata - obsdata
    perc_min = np.percentile(diff, 0.1)
    perc_max = np.percentile(diff, 99.9)
    diff[diff<perc_min] = np.nan
    diff[diff>perc_max] = np.nan
    rmse = np.ma.sqrt(np.ma.mean((np.ma.fix_invalid(diff))**2))

    try:
        return int(np.round(np.ma.filled(np.ma.array(rmse), np.nan)))
    except ValueError:
        return np.ma.filled(np.ma.array(rmse), np.nan)


def plotsett(var):
    """
    """
    dir_ = {
        'map_dbsdiff_abs': {
            'lim': (-400, 400),
            'title': 'Isochrone depth difference (m), \n (modeled-reconstructed)',
            'cmap': 'RdBu_r',
            },
        'map_dbsdiff_rel': {
            'lim': (-10, 10),
            'title': 'Isochrone depth difference, \n (modeled-reconstructed)/rec_uncertainty',
            'cmap': 'RdBu_r',
            },
        'map_obs-layer-diff': {
            'lim': (-400, 400),
            'title': 'OIB layer depth difference (m)',
            'cmap': 'RdBu_r',
            },
        'hice': {
            'lim': (0, 3500),
            'title': 'Ice thickness (m)',
            'cmap': 'viridis',
            },
        'bed_mask': {
            'title': 'Bed properties',
            },
        'hice_diff': {
            'lim_abs': (-400, 400),
            'title': 'Ice thickness difference (m), present day',
            'cmap': 'RdBu_r',
            },
        'uxy_diff': {
            'lim_abs': (-50, 50),
            'title': 'Ice surface velocity difference (m/a), present day',
            'cmap': 'RdBu_r',
            },
        # 'mb_applied': {
        #     'lim': (-2.5, 2.5),
        #     'title': 'plot_nc() example: Applied mass balance (m/a)',
        #     'cmap': 'RdBu',
        #     },
        # 'uxy_s': {
        #     'lim': (1, 2500),
        #     'title': 'Surface velocity (m/a)',
        #     'cmap': 'RdBu_r',
        #     'norm': mpl.colors.LogNorm(),
        #     },
        }

    return dir_[var]


# NOTE REMOVE this function
def reshape(data1, data2, gf):
    """
    NOTE: not tested.
    Needed when comparing ELSA data that has been run on coarser grid (grid factor)
    """
    data2 = at.array_expand_tool(data2, gf)
    data2, data1 = at.make_same_shape(data2, data1)

    return data1, data2


# NOTE REMOVE this function
def interpolate(data, dims, gf):
    """
    """
    if gf == 2:
        xo = np.arange(0.5, dims[1]-1, gf)
        yo = np.arange(0.5, dims[2], gf)
    elif gf == 4:
        xo = np.arange(1.5, dims[1]-1, gf)
        yo = np.arange(1.5, dims[2], gf)[:-1]

    interp = RegularGridInterpolator((xo,yo), data, bounds_error=False, fill_value=None)
    xn = np.arange(0, dims[1], 1)
    yn = np.arange(0, dims[2], 1)
    X, Y = np.meshgrid(xn, yn, indexing='ij')
    new = interp((X, Y))

    return new


# NOTE REMOVE this function
def get_reshaped_ice_mask(ice):
    """
    return ice mask (lat, lon)
    """
    ice_mask = np.ma.masked_where(ice==0, ice)

    return ice_mask.mask



if __name__ == "__main__":
    main()
