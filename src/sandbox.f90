program sandbox

use, intrinsic :: ieee_arithmetic

implicit none

real(8)                 :: aa, bb
logical                 :: cc
real (8), allocatable   :: dd(:,:,:)
real (8)                :: ee(4)
character(len=20), dimension(3)   :: my_strings


aa = ieee_value(aa, ieee_signaling_nan)
bb = ieee_value(bb, ieee_signaling_nan)

cc = .true.

ee = [0, 2, 3, 4]

print *, 'ee', ee

!aa = 5
!bb = 5

!allocate(dd(6, 5, 4))

!print *, 'size dd is ', size(dd, 1), size(dd, 2), size(dd, 3)

allocate(dd(6, 5, 5))

my_strings(1) = "Hello"
my_strings(2) = "World"
my_strings(3) = "Fortran"

print *, my_strings

print *, 'size dd is ', size(dd, 1), size(dd, 2), size(dd, 3)

if (aa .ne. aa*1) then 
  print *, 'aa', aa
  print *, 'bb*1', bb*1
end if

if (cc) then
  print *, 'TRUE'
end if 

aa = 5
write(*,'(A,/,A)') ' ', ''//achar(27)//'[35m simulation complete, saving output '//achar(27)//'[97m', ' '
write(*,*) 'total number of layers: ', ee(3)

aa = 5
aa = aa-5
bb = 2

print *, bb/aa



end program sandbox
