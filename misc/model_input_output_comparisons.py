#compare smb across values for debugging purposes. 

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

#os.chdir(os.path.dirname(sys.argv[0]))
#dir = os.path.dirname(os.path.realpath(__file__))

#load data
aa = np.load("/work2/rob/git-repos/elsa/output/z_bed.npy")
bb = np.load("/work2/rob/git-repos/elsa/output/H_ice.npy")
cc = np.load("/work2/rob/git-repos/elsa/output/ux.npy")
dd = np.load("/work2/rob/git-repos/elsa/output/uy.npy")
ee = np.load("/work2/rob/git-repos/elsa/output/smb.npy")
#ff = np.load('/work2/rob/git-repos/elsa/output/isochrone_project/15-5_no_constraint8000transient/6.npy')

fig, axes = plt.subplots(nrows=2, ncols=3, figsize=(12, 8))

axes[0,0].imshow(aa, cmap='viridis')
axes[0,0].set_title(f'1')
axes[0,0].axis('off')

axes[0,1].imshow(bb, cmap='viridis')
axes[0,1].set_title(f'0')
axes[0,1].axis('off')

axes[0,2].imshow(cc[:,:,10], cmap='viridis', vmin = -25, vmax = 25)
axes[0,2].set_title(f'3')
axes[0,2].axis('off')

axes[1,0].imshow(dd[:,:,10], cmap='viridis', vmin = -25, vmax = 25)
axes[1,0].set_title(f'4')
axes[1,0].axis('off')

axes[1,1].imshow(ee, cmap='viridis')
axes[1,1].set_title(f'5')
axes[1,1].axis('off')

#axes[1,2].imshow(ff, cmap='viridis', vmin=-200, vmax=200)
#axes[1,2].set_title(f'6')
#axes[1,2].axis('off')

plt.tight_layout()
plt.show()



















