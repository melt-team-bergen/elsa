#script to compile (c) and run (r) elsa0

exp_name="example"
nml="example.nml"
layerfile="layer_list.txt"

bash compile-elsa-0_melt.sh
if [ -f ./bin/elsa0 ]; then

  if [ ! -d "./output/$exp_name" ]; then
    echo "creating output directory: ./output/$exp_name/"
    echo " "
    mkdir ./output/$exp_name/
  fi

  echo "copying .nml file to output" 
  echo " "
  cp input/$exp_name/$nml output/$exp_name/$nml

  echo "copying layer_file to output, if it exists" 
  echo " "
  cp input/$exp_name/$layerfile output/$exp_name/$layerfile

  echo "running elsa" #obviously may erroneously be running an old compile 
  echo " "

  #touch file for run status
  if [ -f ./run_complete ]; then
    rm ./output/$exp_name/run_complete
  fi
  touch ./output/$exp_name/run_ongoing
  ./bin/elsa0 --input-file "input/$exp_name/$nml" # print output to terminal
  #./bin/elsa0 --input-file "input/$exp_name/$nml" 2>&1 > "output/$exp_name/out.out" &  # run in background and print output to out.out file in output folder
  rm ./output/$exp_name/run_ongoing
  touch ./output/$exp_name/run_complete

else
  echo "not running elsa" #oh no
fi

