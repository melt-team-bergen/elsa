!These subroutines load input data that may or may not be required for the run.
!Previously located in elsa_isochrones.f90

!therese.rieckh@uib.no, MELT team, University of Bergen, 2023
!robert.law@uib.no, MELT team, University of Bergen, 2023


module load_input_data

  use nml
  use elsa_classes
  use f90getopt
  use, intrinsic :: ieee_arithmetic, only: ieee_is_finite

  implicit none

  contains


  !subroutine to read input options
  subroutine read_arguments(nml_in)

    implicit none

    character(len=512), intent(inout)                   :: nml_in
    type(option_s)                                      :: opts(2)
      opts(1) = option_s("input-file",  .true.,  "i")
      opts(2) = option_s("help",  .false., "h")

    !read options
    if (command_argument_count() .eq. 0) then
      write(*,'(A,/,A)') ''//achar(27)//'[31m No options committed ', 'Use --input-file to specify the incoming .nml file. e.g. &
      ./elsa0 --input-file "example.nml"  ', 'Using "example.nml" as default '//achar(27)//'[97m'
      nml_in = 'input/example.nml'
    else
      do
        select case(getopt("i:h", opts))
            case(char(0)) ! When all options are processed
                exit
            case("i")
                print*, "option input-file/i=",  trim(optarg)
                nml_in = trim(optarg)
            case("h")
                print*, "help-screen" !not in use, but more options can be added here if needed.
        end select
      end do
    end if

  end subroutine read_arguments


  !simple subroutine to load desired isochrones from file. File should have one value per line.
  subroutine load_layer_list(elsa_layers, layer_file)

    implicit none

    character(len=512), intent(in)      :: layer_file
    type(elsa_class), intent(inout)    :: elsa_layers

    integer   :: ii, niso, io
    character(len=50)                  :: line

    if (allocated(elsa_layers%par%layer_list)) deallocate(elsa_layers%par%layer_list)

    niso = 0
    open(1,file=layer_file)
    do
       read(1,'(A)',iostat=io) line
       if (io/=0 .or. line=="") exit
       niso=niso+1
    end do
    close(1)

    print*, 'NUMBER OF ISOCHRONES: ', niso
    allocate(elsa_layers%par%layer_list(niso))

    print *, 'loading layer file: ', layer_file
    open(2,file=layer_file)
    do ii=1,niso
       read(2,*) elsa_layers%par%layer_list(ii)
    end do
    close(2)

  end subroutine load_layer_list



  ! !simple subroutine to load desired isochrones from file. File should have one value per line.
  ! subroutine load_layer_list(elsa_layers, layer_file)

  !   implicit none

  !   character(len=512), intent(in)      :: layer_file
  !   type(elsa_class), intent(inout)    :: elsa_layers

  !   integer   :: ii, nlayers, io
  !   character(len=50)                  :: line

  !   nlayers = 0
  !   open(1,file=layer_file)
  !   do
  !      read(1,'(A)',iostat=io) line
  !      if (io/=0 .or. line=="") exit
  !      nlayers=nlayers+1
  !   end do
  !   close(1)
  !   print*, 'NUMBER OF LAYERS: ', nlayers

  !   !two options. If zeta is set from load netcdf (so layer_file is set as 'na') then simply set as existing values
  !   !otherwise load from layer_file
  !   if ( layer_file .ne. 'NA') then
  !     if (allocated(elsa_layers%par%layer_list)) deallocate(elsa_layers%par%layer_list)
  !     allocate(elsa_layers%par%layer_list(nlayers))

  !     print *, 'loading layer file: ', layer_file
  !       open(2,file=layer_file)
  !       do ii=1,nlayers
  !         read(2,*) elsa_layers%par%layer_list(ii)
  !       end do
  !     close(2)

  !   else
  !     if (allocated(elsa_layers%par%layer_list)) deallocate(elsa_layers%par%layer_list)
  !     allocate(elsa_layers%par%layer_list(1))
  !     elsa_layers%par%layer_list = 1e12 !set incredibly high to avoid seg fault
  !   end if

  ! end subroutine load_layer_list

  

  !simple subroutine to load zeta_aa from file. File should have one value per line.
  subroutine load_zeta(model_input, zeta_file)

    implicit none

    character(len=512), intent(in)      :: zeta_file
    type(model_class), intent(inout)   :: model_input

    integer   :: ii

    !two options. If zeta is set from load netcdf (so zeta_file is set as 'na') then simply set as existing values
    !otherwise load from zeta_file
    if ( zeta_file .ne. 'NA') then
       if (allocated(model_input%grd%par%zeta_aa)) deallocate(model_input%grd%par%zeta_aa)
       allocate(model_input%grd%par%zeta_aa(model_input%grd%par%nz_aa+1))

       print *, 'loading zeta file: ', zeta_file

       open(2,file=zeta_file)
       do ii=1,(model_input%grd%par%nz_aa + 1)
          read(2,*) model_input%grd%par%zeta_aa(ii)
       end do
       close(2)
    end if

  end subroutine load_zeta


  !calculate zeta following calc_zeta in yelmo/src/yelmo_grid.f90
  !presently just linear, so I need to update this to include exponential case
  !and options for users to add their own cases.
  !presently not called anywhere
  subroutine calc_zeta(model_input)

    implicit none

    type(model_class), intent(inout) :: model_input

    integer :: nn

    if (allocated(model_input%grd%par%zeta_aa)) deallocate(model_input%grd%par%zeta_aa)
    allocate(model_input%grd%par%zeta_aa(model_input%grd%par%nz_aa))

    do nn = 1, model_input%grd%par%nz_aa
      model_input%grd%par%zeta_aa(nn) = 0.0 + 1.0*(nn-1)/(model_input%grd%par%nz_aa - 1)
    end do

  end subroutine calc_zeta


 end module load_input_data

