module elsa_isochrones

  use nml
  use m_npy
  use elsa_classes
  use load_input_data
  use, intrinsic :: ieee_arithmetic, only: ieee_is_finite

  implicit none

  contains

  !make a square wave, for dye tracer
  real(dp) function square_wave(pos,width) result(factor)
    integer, intent(in) :: pos
    integer, intent(in) :: width
    factor = (mod(pos,width)-(5d-1*width-5d-1))/abs(mod(pos,width)-(5d-1*width-5d-1))
  end function square_wave


  !this initialises the elsa_layers class
  subroutine elsa_init(elsa_layers, param_file, time_init, time_end, dtt, imax, jmax, dx, dy, h_ice, layer_file)

    implicit none

    type(elsa_class), intent(inout)       :: elsa_layers !elsa class. For initialisation this can be empty
    integer, intent(in)                   :: imax, jmax !size of incoming file/binary from model
    real(dp), intent(in)                   :: dx, dy !x spacing, y spacing
    real(dp), intent(in)                   :: dtt !model time step
    real(dp), intent(in)                  :: time_init, time_end
    real(dp), dimension(imax,jmax), intent(in)   :: h_ice !ice thickness
    character(len=512)                    :: layer_file !layer file location
    character(len=420)                    :: param_file !parameter file location

    integer                               :: ix, iy, iz

    ! Local variables
    integer  :: niso, isochrones, ii
    real(dp) :: compatible(20)


    print *, param_file
    !load parameters
    call nml_read(param_file,"elsa","n_layers_init",elsa_layers%par%init_layers)
    call nml_read(param_file,"elsa","layer_resolution",elsa_layers%par%layer_resolution)
    call nml_read(param_file,"elsa","grid_factor",elsa_layers%par%grid_factor)
    call nml_read(param_file,"elsa","update_factor",elsa_layers%par%update_factor)
    call nml_read(param_file,"elsa","allow_pos_bmb",elsa_layers%par%allow_pos_bmb)
    call nml_read(param_file,"elsa","use_dye_tracer",elsa_layers%par%tracer_on)
    call nml_read(param_file,"elsa","elsa_out",elsa_layers%par%elsa_out)

    !set update time
    elsa_layers%par%update_dt = dtt*elsa_layers%par%update_factor

    if (( elsa_layers%par%layer_resolution .eq. 0.) .and. ( layer_file .eq. 'NA')) then
       write(*,*) "elsa_init:: Error: layer_file is 'NA' and layer_resolution is 0."
       write(*,*) "Set either a path to the layer_file or set a layer_resolution."
       stop
    end if

    if (( elsa_layers%par%layer_resolution .ne. 0.) .and. ( layer_file .ne. 'NA')) then
       write(*,*) "elsa_init:: Error: both layer_file and layer_resolution are set."
       write(*,*) "Set either layer_file to 'NA' or layer_resolution to 0."
       stop
    end if

    
    if ( layer_file .ne. 'NA') then
       call load_layer_list(elsa_layers, layer_file)
       elsa_layers%par%n_layers = size(elsa_layers%par%layer_list)+elsa_layers%par%init_layers + 1
       elsa_layers%par%cutoff = 1000.

       print*, 'layer_list size ', size(elsa_layers%par%layer_list)
       print*, 'update_dt ', elsa_layers%par%update_dt
       do ii=1,size(elsa_layers%par%layer_list)
          if ( mod(elsa_layers%par%layer_list(ii),elsa_layers%par%update_dt) .ne. 0 ) then
             write(*,*) "elsa_init:: Error: coupling frequency (dtt*update_factor) and value in layer_list not compatible."
             print *, 'elsa_layers%par%layer_list = ', elsa_layers%par%layer_list
             print *, 'dtt = ', dtt
             print *, 'elsa_layers%par%update_factor = ', elsa_layers%par%update_factor
             stop
          end if
       end do

    else
       !check that elsa_dt and layer_resolution are compatible
       if ( mod(elsa_layers%par%layer_resolution,dtt*elsa_layers%par%update_factor) .ne. 0.) then
          write(*,*) "elsa_init:: Error: coupling frequency (dtt*update_factor) and layer resolution not compatible."
          print *, 'elsa_layers%par%layer_resolution = ', elsa_layers%par%layer_resolution
          print *, 'dtt = ', dtt
          print *, 'elsa_layers%par%update_factor = ', elsa_layers%par%update_factor
          print *, 'mod', mod(elsa_layers%par%layer_resolution,dtt*elsa_layers%par%update_factor)
          stop
       end if

       !check that simulation length and layer_resolution are compatible
       if ( mod((time_end-time_init),elsa_layers%par%layer_resolution) .ne. 0.) then
          write(*,*) "elsa_init:: Error: isochrone layer resolution inconsistent with length of simulation."
          stop
       end if

       ! NOTE: number of isochrones (interfaces between layers)
       niso = int((time_end-time_init)/elsa_layers%par%layer_resolution) - 1
       elsa_layers%par%n_layers = int(niso+elsa_layers%par%init_layers + 1)

       if (allocated(elsa_layers%par%layer_list)) deallocate(elsa_layers%par%layer_list)
       allocate(elsa_layers%par%layer_list(niso))
       isochrones = time_init + elsa_layers%par%layer_resolution
       do ii=1,niso
          elsa_layers%par%layer_list(ii) = isochrones
          isochrones = isochrones + elsa_layers%par%layer_resolution
       end do
           ! compute cutoff value for advection
       elsa_layers%par%cutoff = 100.+10.*elsa_layers%par%update_factor+elsa_layers%par%layer_resolution/10.

    end if


    !determine the size of the x/y grid
    if (elsa_layers%par%grid_factor .eq. 1) then
      elsa_layers%par%nx_iso = imax
      elsa_layers%par%ny_iso = jmax
      elsa_layers%par%dx_iso = dx
      elsa_layers%par%dy_iso = dy
   else
      elsa_layers%par%nx_iso = floor(dble(imax)/elsa_layers%par%grid_factor)
      elsa_layers%par%ny_iso = floor(dble(jmax)/elsa_layers%par%grid_factor)

      if ((mod(imax,elsa_layers%par%grid_factor) .ne. 0) .or. (mod(jmax,elsa_layers%par%grid_factor) .ne. 0)) then
        write(*,*) "elsa_init:: Warning: iso grid with defined grid_factor does not cover all of original grid."
      end if

      elsa_layers%par%dx_iso = dx * elsa_layers%par%grid_factor
      elsa_layers%par%dy_iso = dy * elsa_layers%par%grid_factor
    end if

    !print some information
    write(*,"(A)") "elsa grid:"
    write(*,"(A,I20)") "  grid_factor = ",elsa_layers%par%grid_factor
    write(*,"(A,I20)") "  nx_iso      = ",elsa_layers%par%nx_iso
    write(*,"(A,I20)") "  ny_iso      = ",elsa_layers%par%ny_iso
    write(*,"(A,I20)") "  dx_iso      = ",int(elsa_layers%par%dx_iso)
    write(*,"(A,I20)") "  dy_iso      = ",int(elsa_layers%par%dy_iso)
    write(*,"(A,I20)") "  n_layers (including init layers)    = ",elsa_layers%par%n_layers !note this is the layers in the elsa class, not the model input
    write(*,*)         "  isochrone surfaces  = ",elsa_layers%par%layer_list !note this is the layers in the elsa class, not the model input

    !deallocate grid
    call elsa_dealloc(elsa_layers%now)

    !allocate grid
    allocate(elsa_layers%now%vx_iso(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso,elsa_layers%par%n_layers))
    allocate(elsa_layers%now%vy_iso(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso,elsa_layers%par%n_layers))
    allocate(elsa_layers%now%dsum_iso(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso,elsa_layers%par%n_layers))
    allocate(elsa_layers%now%d_iso(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso,elsa_layers%par%n_layers))
    if (elsa_layers%par%tracer_on) then
      allocate(elsa_layers%now%tracer_iso(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso,elsa_layers%par%n_layers))
    end if
    allocate(elsa_layers%now%H_ice_iso(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso))
    allocate(elsa_layers%now%sum_smb(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso))
    allocate(elsa_layers%now%sum_bmb(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso))
    allocate(elsa_layers%now%misc_1(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso, elsa_layers%par%n_layers))
    elsa_layers%now%sum_smb = 0d0
    elsa_layers%now%sum_bmb = 0d0


    !interp ice thickness
    if (elsa_layers%par%grid_factor .eq. 1) then
      elsa_layers%now%H_ice_iso = h_ice
    else
      call regrid_xy(h_ice,imax,jmax,elsa_layers%par%grid_factor, &
        elsa_layers%now%H_ice_iso,elsa_layers%par%nx_iso,elsa_layers%par%ny_iso)
    end if

    !fill the first <init_layers> layers with equidistant spacing:
    elsa_layers%now%top_layer = elsa_layers%par%init_layers

    do ix=1,elsa_layers%par%nx_iso,1
      do iy=1,elsa_layers%par%ny_iso,1

        elsa_layers%now%d_iso(ix,iy,1)    = elsa_layers%now%H_ice_iso(ix,iy) / elsa_layers%now%top_layer
        elsa_layers%now%dsum_iso(ix,iy,1) = elsa_layers%now%d_iso(ix,iy,1)

        do iz=2,elsa_layers%now%top_layer,1
          elsa_layers%now%d_iso(ix,iy,iz)    = elsa_layers%now%H_ice_iso(ix,iy) / elsa_layers%now%top_layer
          elsa_layers%now%dsum_iso(ix,iy,iz) = elsa_layers%now%dsum_iso(ix,iy,iz-1) + elsa_layers%now%d_iso(ix,iy,iz)
        end do
      end do
    end do

    if (elsa_layers%par%tracer_on) then
      ! Fill passive dye tracer for all times:
      do ix=1,elsa_layers%par%nx_iso,1
        elsa_layers%now%tracer_iso(ix,:,1) = square_wave(ix,20)
      end do
      do iy=1,elsa_layers%par%ny_iso,1
        elsa_layers%now%tracer_iso(:,iy,1) = elsa_layers%now%tracer_iso(:,iy,1) * square_wave(iy,20)
      end do
      do iz=2,elsa_layers%par%n_layers,1
        elsa_layers%now%tracer_iso(:,:,iz) = elsa_layers%now%tracer_iso(:,:,1) * square_wave(iz,20)
      end do
    end if

    write(*,*) "isochrones intialized"

    ! add first new empty layer here to start filling with smb
    call elsa_add_layer(elsa_layers)

  end subroutine elsa_init


  !deallocates grids
  subroutine elsa_dealloc(grd)

    implicit none

    type(elsa_state_class), intent(inout) :: grd

    ! Axis vectors
    if (allocated(grd%vx_iso))      deallocate(grd%vx_iso)
    if (allocated(grd%vy_iso))      deallocate(grd%vy_iso)
    if (allocated(grd%dsum_iso))    deallocate(grd%dsum_iso)
    if (allocated(grd%d_iso))       deallocate(grd%d_iso)
    if (allocated(grd%tracer_iso))  deallocate(grd%tracer_iso)
    if (allocated(grd%sum_smb))     deallocate(grd%sum_smb)
    if (allocated(grd%sum_bmb))     deallocate(grd%sum_bmb)

    return
  end subroutine elsa_dealloc


  !time step isochrones
  subroutine elsa_update(model_input, elsa_layers, update_dt)

    implicit none

    type(model_class), intent(in)       :: model_input
    type(elsa_class), intent(inout)     :: elsa_layers
    real(dp), intent(in)                :: update_dt

    integer       :: ix, iy, iz, ij, imax, jmax
    real(dp), dimension(elsa_layers%par%nx_iso, elsa_layers%par%ny_iso)  :: d_iso_tmp, tracer_iso_tmp

    imax = elsa_layers%par%nx_iso
    jmax = elsa_layers%par%ny_iso

    !update thickness, regrid if required
    if (elsa_layers%par%grid_factor .eq. 1) then
      elsa_layers%now%H_ice_iso = model_input%tpo%now%H_ice
    else
      call regrid_xy(model_input%tpo%now%H_ice,model_input%grd%par%nx,model_input%grd%par%ny, &
        elsa_layers%par%grid_factor, &
      elsa_layers%now%H_ice_iso,elsa_layers%par%nx_iso,elsa_layers%par%ny_iso)
    end if

    !normalise d_iso so that its sum is equal to H_ice, this keeps model drift in check
    elsa_layers%now%d_iso(:,:,1:elsa_layers%now%top_layer) = &
        normalise_d(elsa_layers%now%d_iso(:,:,1:elsa_layers%now%top_layer),elsa_layers%now%H_ice_iso, &
        imax,jmax,elsa_layers%now%top_layer)

    !update layer height over bedrock (dsum_iso)
    do ix=1,imax,1
      do iy=1,jmax,1

        !first layer
        elsa_layers%now%dsum_iso(ix,iy,1) = elsa_layers%now%d_iso(ix,iy,1)

        !subsequent layers
        do iz=2,elsa_layers%now%top_layer,1
          elsa_layers%now%dsum_iso(ix,iy,iz) = elsa_layers%now%dsum_iso(ix,iy,iz-1) + elsa_layers%now%d_iso(ix,iy,iz)
        end do
      end do
    end do

    !interpolate velocities to isochrone grid
    call interp_to_iso(model_input, elsa_layers)


!$OMP PARALLEL DO PRIVATE(d_iso_tmp,tracer_iso_tmp)
    do iz=1, elsa_layers%now%top_layer, 1
      call advect_2D(imax,jmax,dble(update_dt)/dble(elsa_layers%par%dx_iso), &
        dble(update_dt)/dble(elsa_layers%par%dy_iso), elsa_layers%now%d_iso(:,:,iz), &
        elsa_layers%now%tracer_iso(:,:,iz), elsa_layers%now%vx_iso(:,:,iz), &
        elsa_layers%now%vy_iso(:,:,iz), elsa_layers%par%tracer_on, d_iso_tmp, tracer_iso_tmp, elsa_layers%par%cutoff)

!$OMP CRITICAL
      elsa_layers%now%d_iso(:,:,iz) = d_iso_tmp
      if (elsa_layers%par%tracer_on) then
        elsa_layers%now%tracer_iso(:,:,iz) = tracer_iso_tmp
      end if
!$OMP END CRITICAL

    end do
!$OMP END PARALLEL DO

    return
  end subroutine elsa_update


  subroutine advect_2D(imax, jmax, dt_dx, dt_dy, d_iso, tracer_iso, &
     vx_iso, vy_iso, tracer_on, d_iso_out, tracer_iso_out, cutoff)

    implicit none

    logical                                         :: tracer_on
    integer                       , intent(in)      :: imax,jmax
    real(dp)                      , intent(in)      :: dt_dx,dt_dy
    real(dp), dimension(imax,jmax), intent(in)      :: vx_iso, vy_iso
    real(dp), dimension(imax,jmax), intent(in)      :: d_iso
    real(dp), dimension(imax,jmax), intent(in)      :: tracer_iso
    real(dp), dimension(imax,jmax), intent(out)     :: tracer_iso_out, d_iso_out
    real(dp)                      , intent(in)      :: cutoff

    integer         :: ij,ix,iy,icount,kcount


#include "lisf.h"
  LIS_INTEGER              :: n_lis,ierr,is,ie
  LIS_MATRIX               :: A_lis
  LIS_VECTOR               :: b_lis,x_lis
  LIS_VECTOR               :: b_tracer_lis,x_tracer_lis
  LIS_SOLVER               :: solver_lis
  LIS_SCALAR               :: A_lis_diag
  LIS_SCALAR               :: A_lis_ip1
  LIS_SCALAR               :: A_lis_im1
  LIS_SCALAR               :: A_lis_jp1
  LIS_SCALAR               :: A_lis_jm1
  LIS_SCALAR, allocatable  :: tmp_var(:)
  LIS_SCALAR, allocatable  :: tmp_tracer_var(:)

    n_lis = imax * jmax

    call lis_initialize(ierr)

    call lis_matrix_create(LIS_COMM_WORLD,A_lis,ierr)
    call lis_vector_create(LIS_COMM_WORLD,b_lis,ierr)
    call lis_vector_create(LIS_COMM_WORLD,x_lis,ierr)

    if (tracer_on) then
      call lis_vector_create(LIS_COMM_WORLD,b_tracer_lis,ierr)
      call lis_vector_create(LIS_COMM_WORLD,x_tracer_lis,ierr)
    end if

    call lis_matrix_set_size(A_lis,0,n_lis,ierr)
    call lis_vector_set_size(b_lis,0,n_lis,ierr)
    call lis_vector_set_size(x_lis,0,n_lis,ierr)

    if (tracer_on) then
      call lis_vector_set_size(b_tracer_lis,0,n_lis,ierr)
      call lis_vector_set_size(x_tracer_lis,0,n_lis,ierr)
    end if

  ! populate matrix and vector elements:
  ij = 0
  do iy=1,jmax,1
    do ix=1,imax,1
      ij = ij+1

      A_lis_diag = 1d0! main diagonal (additional terms below)

      !boundary conditions (keep layer thickness unchanged):
      if ((ix .eq. 1) .or. (ix .eq. imax) .or. (iy .eq. 1) .or. (iy .eq. jmax)) then
      !if ((ix .eq. 1) .or. (iy .eq. 1)) then !T_R update , RL following this leads to lots of errors, e.g. ...
      !lis_matrix.c(737) : lis_matrix_set_value : error ILL_ARG :i(=19071) or j(=19252) are larger than global n=(19186)
        call lis_matrix_set_value(LIS_INS_VALUE, ij ,ij,dble(A_lis_diag),A_lis,ierr)! main diagognal
        call lis_vector_set_value(LIS_INS_VALUE,ij,dble(d_iso(ix,iy)),b_lis,ierr) ! added dble() to ensure no data type overflow (GG)
        !call lis_vector_set_value(LIS_INS_VALUE,ij,0_prec,b_lis,ierr)

      else
        !NOTE: do not advect if vx or vy between -0.1 and 0.1 m per year. Using 0 is unstable
        if (vx_iso(ix,iy) .gt. 0.1d0) then
        ! if (vx_iso(ix,iy) .gt. 0.5d0) then
          !A_lis_diag = A_lis_diag + 0.5d0*dt_dx * (vx_iso(ix,iy) + vx_iso(ix+1,iy))
          !A_lis_ip1  = 0d0                                                    ! i+1
          !A_lis_im1  = -0.5d0*dt_dx * (vx_iso(ix-1,iy) + vx_iso(ix,iy))       ! i-1
          A_lis_diag = A_lis_diag + (dt_dx * vx_iso(ix,iy)) !T_R update
          A_lis_ip1  = 0d0                            ! i+1 !T_R update
          A_lis_im1  = -dt_dx * vx_iso(ix-1,iy)       ! i-1 !T_R update


        elseif (vx_iso(ix,iy) .lt. -0.1d0) then
        ! elseif (vx_iso(ix,iy) .lt. -0.5d0) then
          !A_lis_diag = A_lis_diag - 0.5d0*dt_dx * (vx_iso(ix,iy) + vx_iso(ix-1,iy))
          !A_lis_ip1  = 0.5d0*dt_dx * (vx_iso(ix+1,iy) + vx_iso(ix,iy))
          A_lis_diag = A_lis_diag - (dt_dx * vx_iso(ix-1,iy)) !T_R update
          A_lis_ip1  = dt_dx * vx_iso(ix,iy) !T_R update
          A_lis_im1  = 0d0

        else
          A_lis_ip1 = 0d0
          A_lis_im1 = 0d0
        end if

        if (vy_iso(ix,iy) .gt. 0.1d0) then
        ! if (vy_iso(ix,iy) .gt. 0.5d0) then
          !A_lis_diag = A_lis_diag + 0.5d0*dt_dy * (vy_iso(ix,iy) + vy_iso(ix,iy+1))
          !A_lis_jp1  = 0d0                                                    ! j+1
          !A_lis_jm1  = -0.5d0*dt_dy * (vy_iso(ix,iy-1) + vy_iso(ix,iy))       ! j-1
          A_lis_diag = A_lis_diag + (dt_dy * vy_iso(ix,iy)) !T_R update
          A_lis_jp1  = 0d0                            ! j+1 !T_R update
          A_lis_jm1  = -dt_dy * vy_iso(ix,iy-1)       ! j-1 !T_R update


        elseif (vy_iso(ix,iy) .lt. -0.1d0) then
        ! elseif (vy_iso(ix,iy) .lt. -0.5d0) then
          !A_lis_diag = A_lis_diag - 0.5d0*dt_dy * (vy_iso(ix,iy) + vy_iso(ix,iy-1))
          !A_lis_jp1  = 0.5d0*dt_dy * (vy_iso(ix,iy+1) + vy_iso(ix,iy))
          A_lis_diag = A_lis_diag - (dt_dy * vy_iso(ix,iy-1)) !T_R update
          A_lis_jp1  = dt_dy * vy_iso(ix,iy) !T_R update
          A_lis_jm1  = 0d0 !T_R update
        else
          A_lis_jp1  = 0d0
          A_lis_jm1  = 0d0
        end if

        !The next line just fills the main diagonal with zeros, not sure why it is in here !T_R update
        !call lis_matrix_set_value(LIS_INS_VALUE,(iy-1)*imax+ix,(iy-1)*imax+ix,dble(0d0),A_lis,ierr)
        call lis_matrix_set_value(LIS_INS_VALUE,ij,ij,dble(A_lis_diag),A_lis,ierr)          ! main diagognal
        call lis_matrix_set_value(LIS_INS_VALUE,ij,ij+1,dble(A_lis_ip1),A_lis,ierr)         ! i+1
        call lis_matrix_set_value(LIS_INS_VALUE,ij,ij-1,dble(A_lis_im1),A_lis,ierr)         ! i-1
        call lis_matrix_set_value(LIS_INS_VALUE,ij,ij+imax,dble(A_lis_jp1),A_lis,ierr)      ! j+1
        call lis_matrix_set_value(LIS_INS_VALUE,ij,ij-imax,dble(A_lis_jm1),A_lis,ierr)      ! j-1

        call lis_vector_set_value(LIS_INS_VALUE,ij,dble(d_iso(ix,iy)),x_lis,ierr)  ! initial guess
        call lis_vector_set_value(LIS_INS_VALUE,ij,dble(d_iso(ix,iy)),b_lis,ierr)

        if (tracer_on) then
          call lis_vector_set_value(LIS_INS_VALUE,ij,dble(tracer_iso(ix,iy)),x_tracer_lis,ierr)! initial guess
          call lis_vector_set_value(LIS_INS_VALUE,ij,dble(tracer_iso(ix,iy)),b_tracer_lis,ierr)
        end if

      end if

    end do
  end do

  call lis_matrix_set_type(A_lis,LIS_MATRIX_CSR,ierr)
  call lis_matrix_assemble(A_lis,ierr)

  call lis_vector_get_range(b_lis,is,ie,ierr)

  !set up LIS solver
  call lis_solver_create(solver_lis,ierr)
  call lis_solver_set_option('-i bicg -p jacobi -maxiter 1000 -tol 1.0e-12 -initx_zeros false',solver_lis,ierr)
  call lis_solve(A_lis,b_lis,x_lis,solver_lis,ierr)
  if (tracer_on) then
    call lis_solve(A_lis,b_tracer_lis,x_tracer_lis,solver_lis,ierr)
  end if

  allocate(tmp_var(ie-is))
  call lis_vector_get_values(x_lis,1,ie-is,tmp_var,ierr)
  if (tracer_on) then
    allocate(tmp_tracer_var(ie-is))
    call lis_vector_get_values(x_tracer_lis,1,ie-is,tmp_tracer_var,ierr)
  end if

  !clean up
  call lis_matrix_destroy(A_lis,ierr)
  call lis_vector_destroy(b_lis,ierr)
  call lis_vector_destroy(x_lis,ierr)
  if (tracer_on) then
    call lis_vector_destroy(b_tracer_lis,ierr)
    call lis_vector_destroy(x_tracer_lis,ierr)
  end if
  call lis_solver_destroy(solver_lis,ierr)
  call lis_finalize(ierr)

  !write solution back to array
  icount = 0
  kcount = 0
  ij = 0


  do iy=1,jmax,1
    do ix=1,imax,1
        ij = ij+1
        !if d_iso changes by more than cutoff m: use old d_iso
        if (tmp_var(ij)-d_iso(ix,iy) .gt. cutoff) then
            write(*,*) 'NOTE: layer thickness (m) increase more than', cutoff
            write(*,*) '      d_iso, tmp_var, ix, iy: ', d_iso(ix,iy), tmp_var(ij), ix, iy
            tmp_var(ij) = d_iso(ix,iy)
            icount = icount+1
        else if (.not. ieee_is_finite(tmp_var(ij))) then
        ! if (.not. ieee_is_finite(tmp_var(ij))) then
           write(*,*) 'NOTE: invalid value for tmp_var', tmp_var(ij)
           tmp_var(ij) = d_iso(ix,iy)
           kcount = kcount+1
        end if
        d_iso_out(ix,iy) = max(tmp_var(ij),0d0)
    end do
  end do

  ! print *, 'icount: ', icount, ', kcount: ', kcount

  deallocate(tmp_var)

  if (tracer_on) then
    ij = 0
    do iy=1,jmax,1
      do ix=1,imax,1
        ij = ij+1
        tracer_iso_out(ix,iy) = tmp_tracer_var(ij)
      end do
    end do
    deallocate(tmp_tracer_var)
  end if

  end subroutine advect_2D


  subroutine regrid_xy(var_in,nx_in,ny_in,step_size,var_out,nx_out,ny_out)
    !transfer variables from one lateral grid to a coarser one
    implicit none

    integer                                           :: ix,iy,sx,sy,ix_new,iy_new
    integer, intent(IN)                               :: nx_in,ny_in,nx_out,ny_out,step_size
    real(dp)                                        :: var_sum
    real(dp), dimension(nx_in,ny_in), intent(IN)    :: var_in
    real(dp), dimension(nx_out,ny_out), intent(OUT) :: var_out

    do ix=1,nx_out,1
       do iy=1,ny_out,1
         var_sum = 0
         do sx=1,step_size,1
            ix_new = step_size*(ix-1)+sx
            do sy=1,step_size,1
               iy_new = step_size*(iy-1)+sy
               var_sum = var_sum + var_in(ix_new,iy_new)
            end do
         end do
         var_out(ix,iy) = (1d0 / step_size**2) * var_sum
      end do
    end do

    return
  end subroutine regrid_xy


  !interpolate xy velocities to isochrone grid
  subroutine interp_to_iso(model_input, elsa_layers)

    implicit none

    type(model_class), intent(in)     :: model_input
    type(elsa_class), intent(inout)   :: elsa_layers

    integer       :: ix, iy, iz, iz_zeta
    real(dp), dimension(model_input%grd%par%nz_aa)     :: dsum_in ![m] cummulative sum of zeta_aa layer thickness
    real(dp), dimension(elsa_layers%par%nx_iso, elsa_layers%par%ny_iso, model_input%grd%par%nz_aa) &
      :: ux_regrid, uy_regrid ![m s-1] regridded ux and uy

    elsa_layers%now%vx_iso = 0d0
    elsa_layers%now%vy_iso = 0d0

    !regrid velocities to elsa grid if required
    if (elsa_layers%par%grid_factor .eq. 1) then
      ux_regrid = model_input%dyn%now%ux
      uy_regrid = model_input%dyn%now%uy
    else
      do iz=1,model_input%grd%par%nz_aa,1
        call regrid_xy(model_input%dyn%now%ux(:,:,iz),model_input%grd%par%nx,model_input%grd%par%ny, &
          elsa_layers%par%grid_factor, ux_regrid(:,:,iz),elsa_layers%par%nx_iso,elsa_layers%par%ny_iso)
        call regrid_xy(model_input%dyn%now%uy(:,:,iz),model_input%grd%par%nx,model_input%grd%par%ny, &
          elsa_layers%par%grid_factor, uy_regrid(:,:,iz),elsa_layers%par%nx_iso,elsa_layers%par%ny_iso)
      end do
    end if


    !RL for now allocating/deallocating zeta_aa based on its implementation in yelmo/src/yelmo_grid.f90
    !but looking for a more universal way of obtaining this from model output
    !this is where allocation was, it's now in elsa0.f90

    !interpolate on each column
    do ix=1,elsa_layers%par%nx_iso,1
      do iy=1,elsa_layers%par%ny_iso,1

        !run only where ice thickness is larger than zero to avoid division by zero:
        if (elsa_layers%now%H_ice_iso(ix,iy) .gt. 0d0) then

          dsum_in = elsa_layers%now%H_ice_iso(ix,iy) * model_input%grd%par%zeta_aa

          do iz=1,elsa_layers%now%top_layer,1

            ! find the first zeta layer above the current isochronal layer:
            iz_zeta = 1
            do while ((dsum_in(iz_zeta) .lt. elsa_layers%now%dsum_iso(ix,iy,iz)) .and. (iz_zeta .lt. model_input%grd%par%nz_aa))
              iz_zeta = iz_zeta + 1
            end do

            ! linear interpolation (except for upper and lower boundaries):
            if ((iz_zeta .gt. 1) .and. (dsum_in(iz_zeta) .gt. elsa_layers%now%dsum_iso(ix,iy,iz))) then
              elsa_layers%now%vx_iso(ix,iy,iz) = &
                  (ux_regrid(ix,iy,iz_zeta-1)*(dsum_in(iz_zeta)-elsa_layers%now%dsum_iso(ix,iy,iz)) &
                  + ux_regrid(ix,iy,iz_zeta)*(elsa_layers%now%dsum_iso(ix,iy,iz)-dsum_in(iz_zeta-1))) &
                  /(dsum_in(iz_zeta)-dsum_in(iz_zeta-1))
              elsa_layers%now%vy_iso(ix,iy,iz) = &
                  (uy_regrid(ix,iy,iz_zeta-1)*(dsum_in(iz_zeta)-elsa_layers%now%dsum_iso(ix,iy,iz)) &
                  + uy_regrid(ix,iy,iz_zeta)*(elsa_layers%now%dsum_iso(ix,iy,iz)-dsum_in(iz_zeta-1))) &
                  /(dsum_in(iz_zeta)-dsum_in(iz_zeta-1))
            else
              elsa_layers%now%vx_iso(ix,iy,iz) = ux_regrid(ix,iy,iz_zeta)
              elsa_layers%now%vy_iso(ix,iy,iz) = uy_regrid(ix,iy,iz_zeta)
            end if

            !check for NaNs within vx grid
            if (elsa_layers%now%vx_iso(ix,iy,iz) .ne. elsa_layers%now%vx_iso(ix,iy,iz)*1d0) then
              print *, 'error, NaNs within vx_iso grid'
              print *, elsa_layers%now%vx_iso(ix,iy,iz)
              stop
            end if

          end do

        else

          elsa_layers%now%vx_iso(ix,iy,:) = 0d0
          elsa_layers%now%vy_iso(ix,iy,:) = 0d0

        end if

      end do
    end do

    return
  end subroutine interp_to_iso


  !normalise d_iso so that its sum equals H_ice, completed each update cycle
  function normalise_d(d_in, h_in, imax, jmax, top_layer) result(d_out)

    integer, intent(in)             :: imax, jmax, top_layer

    real(dp), dimension(imax, jmax, top_layer), intent(in)   :: d_in
    real(dp), dimension(imax, jmax), intent(in)              :: h_in
    real(dp), dimension(imax, jmax, top_layer)               :: d_out

    integer       :: ix, iy, iz
    real(dp)       :: sum_iz

    do ix=1,imax,1
      do iy=1,jmax,1
        sum_iz = sum(d_in(ix,iy,1:top_layer))

        !if not an empty column:
        if (sum_iz .gt. 0_8) then
          do iz=1,top_layer,1
             d_out(ix,iy,iz) = d_in(ix,iy,iz) * h_in(ix,iy)/sum_iz
             if (.not. ieee_is_finite(d_out(ix,iy,iz))) then
                print *, 'invalid value in normalization (infinite)'
                print *, 'ix iy iz, ', ix, iy, iz
                print *, 'sum_iz is, ', sum_iz
                print *, 'h_in(ix, iy) is, ', h_in(ix,iy)
                print *, 'd_in(ix, iy, iz) is, ', d_in(ix,iy,iz)
             end if

          end do

        !if column does not contain any ice
        else
          d_out(ix,iy,1:top_layer) = 0_8
        end if

      end do
    end do
  return
  end function normalise_d


  !add a new layer
  subroutine elsa_add_layer(elsa_layers)

    implicit none

    type(elsa_class), intent(inout)                                     :: elsa_layers

    elsa_layers%now%top_layer = elsa_layers%now%top_layer + 1

    return
   end subroutine elsa_add_layer


  !keep track of cumulative surface mass balance between layer_resolution time steps
  subroutine elsa_add_smb(elsa_layers, update_dt, imax, jmax, smb)

    implicit none

    type(elsa_class), intent(inout)                                     :: elsa_layers
    real(dp), intent(in)                                                :: update_dt
    integer, intent(in)                                                 :: imax, jmax
    real(dp), dimension(imax,jmax), intent(in)                           :: smb
    real(dp), dimension(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso)   :: smb_regrid

    ! Local variables
    integer     :: ix, iy, iz
    real(dp)     :: smb_pos, smb_neg       ![m] cummulative smb
    real(dp)     :: d_tmp                  ![] temporary variable for d_iso (which is layer thickness as fraction of total ice thickness)

    !interpolate SMB to elsa grid:
    if (elsa_layers%par%grid_factor .eq. 1) then
      smb_regrid = smb
    else
      call regrid_xy(smb,imax,jmax,elsa_layers%par%grid_factor, &
          smb_regrid,elsa_layers%par%nx_iso,elsa_layers%par%ny_iso)
    end if

    elsa_layers%now%sum_smb = smb_regrid*update_dt

    do ix=1,elsa_layers%par%nx_iso,1
      do iy=1,elsa_layers%par%ny_iso,1

        ! Split into accumulation and ablation:
        smb_pos = (elsa_layers%now%sum_smb(ix,iy) + abs(elsa_layers%now%sum_smb(ix,iy)))/2d0
        smb_neg = (elsa_layers%now%sum_smb(ix,iy) - abs(elsa_layers%now%sum_smb(ix,iy)))/2d0

        ! positive mass balance:
        elsa_layers%now%d_iso(ix,iy,elsa_layers%now%top_layer) = elsa_layers%now%d_iso(ix,iy,elsa_layers%now%top_layer) + smb_pos

        ! negative surface mass balance:
        do iz=elsa_layers%now%top_layer,1,-1

          if (smb_neg .lt. 0d0) then

            if (elsa_layers%now%d_iso(ix,iy,iz) .gt. 0d0) then

              d_tmp = elsa_layers%now%d_iso(ix,iy,iz)
              ! remove as much material as possible from the current layer:
              elsa_layers%now%d_iso(ix,iy,iz) = elsa_layers%now%d_iso(ix,iy,iz) + max(smb_neg, -1d0*d_tmp)
              ! remove the same amount from the melting term to keep track how much more needs to be removed:
              smb_neg = smb_neg + (d_tmp-elsa_layers%now%d_iso(ix,iy,iz))
            end if

          else
            exit ! terminates iz loop
          end if

        end do
      end do
    end do

    return
   end subroutine elsa_add_smb


  !keep track of cumulative basal mass balance between layer_resolution time steps
  subroutine elsa_add_bmb(elsa_layers,update_dt,imax,jmax,bmb)

    implicit none

    type(elsa_class), intent(inout)                                     :: elsa_layers
    real(dp), intent(in)                                                :: update_dt
    integer, intent(in)                                                 :: imax, jmax
    real(dp), dimension(imax,jmax), intent(in)                           :: bmb
    real(dp), dimension(elsa_layers%par%nx_iso,elsa_layers%par%ny_iso)   :: bmb_regrid

    ! Local variables
    integer     :: ix,iy,iz
    real(dp)  :: bmb_pos, bmb_neg!      [m]  cumulative BMB
    real(dp)  :: d_tmp!                 [m]  temporary variable for d_iso

    !interpolate SMB to elsa grid:
    if (elsa_layers%par%grid_factor .eq. 1) then
      bmb_regrid = bmb
    else
      call regrid_xy(bmb,imax,jmax,elsa_layers%par%grid_factor, &
          bmb_regrid,elsa_layers%par%nx_iso,elsa_layers%par%ny_iso)
    end if

    elsa_layers%now%sum_bmb = bmb_regrid*update_dt

    do ix=1,elsa_layers%par%nx_iso,1
      do iy=1,elsa_layers%par%ny_iso,1

        !basal freezing: add positive bmb to bottom layer. No new *layer* is added
        if (elsa_layers%par%allow_pos_bmb) then
           bmb_pos = (elsa_layers%now%sum_bmb(ix,iy) + abs(elsa_layers%now%sum_bmb(ix,iy)))/2d0
           elsa_layers%now%d_iso(ix,iy,1) = elsa_layers%now%d_iso(ix,iy,1) + bmb_pos
           !elsa_layers%now%misc_1(ix,iy) = elsa_layers%now%d_iso(ix,iy,1) + bmb_pos
        end if

        bmb_neg = (elsa_layers%now%sum_bmb(ix,iy) - abs(elsa_layers%now%sum_bmb(ix,iy)))/2d0

        !basal melting:
        do iz=1,elsa_layers%now%top_layer,1
           if (bmb_neg .lt. 0d0) then
              if (elsa_layers%now%d_iso(ix,iy,iz) .gt. 0d0) then
                 d_tmp = elsa_layers%now%d_iso(ix,iy,iz)
                 !remove as much material as possible from the current layer
                 elsa_layers%now%d_iso(ix,iy,iz) = elsa_layers%now%d_iso(ix,iy,iz) + max(bmb_neg, -1d0*d_tmp)
                 !remove the same amount from the melting term to keep track how much more needs to be removed:
                 bmb_neg = bmb_neg + (d_tmp-elsa_layers%now%d_iso(ix,iy,iz))
              end if

           else
              exit
           end if

          end do
      end do
    end do
    return
   end subroutine elsa_add_bmb


  !determines if time_in time step matches target
  logical function its_time(time_in, target, time_step) result(go_on)
    real(4), intent(in)    :: time_in, target, time_step
    go_on  = mod(time_in,target) .lt. time_step
  end function its_time


  subroutine hola()
    write(*,*) 'hola'
  end subroutine hola

end module elsa_isochrones
