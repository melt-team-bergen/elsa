!this is a simplification of the classes from yelmo/rc/yelmo_def.f90 adapted for use only with ELSA-0
!as such, it may seem slightly more complicated than strictly necessary, but this is done to maintain
!compatibility with the previous ELSA implementation.

!the two central classes here are:
!
!       model_class:
!               model_param_class
!               topo_class:
!                       topo_param_class
!                       topo_state_class
!               grid_class:
!                       grid_param_class
!                       grid_state_class
!               dyn_class:
!                       dyn_state_class
!
!       elsa_class:
!               elsa_state_class
!               elsa_param_class
!
!model_class is to take input from ice sheet model output
!elsa_class is for use within ELSA


module elsa_classes

  integer, parameter :: dp = kind(1.d0) !https://fortranwiki.org/fortran/show/Real+precision

  !FROM YELMO ################################################################################
  !==================tpo [topography]=====================!
  type topo_param_class
    real(dp)        :: time
  end type

  type topo_state_class
    real(dp), allocatable   :: H_ice(:,:)    !ice thickness (2D, m)
    real(dp), allocatable   :: z_bed(:,:)    !bedrock elevation (m)
  end type

  type topo_class
    type(topo_param_class) :: par          !parameters
    type(topo_state_class) :: now          !variables
  end type

  !=================dyn [dynamics]====================!
  type dyn_state_class
    real(dp), allocatable   :: ux(:,:,:)    !x velocity (m a-1)
    real(dp), allocatable   :: uy(:,:,:)    !y velocity (m a-1)
    real(dp), allocatable   :: uz(:,:,:)    !z velocity (m a-1)
  end type

  type dyn_class
    type(dyn_state_class)   :: now          !variables
  end type

  !================grd ===================!
  type grd_param_class
    integer        :: nx, ny                !number of grid elements ()
    integer        :: nz_aa                 !number of vertical layers
    real(dp), allocatable :: zeta_aa(:)      !"layer centers (aa-nodes), plus base and surface: nz_aa points"
    real(dp)        :: dx, dy                ![m] x and y grid spacing
  end type

  type grd_state_class
    real(dp), allocatable   :: d_iso(:,:,:)     !spacing of vertical layers
  end type

  type grd_class
    type(grd_param_class)   :: par
    type(grd_state_class)   :: now          !variables
  end type

  !================mass balance===========!
  type mb_state_class
    real(dp), allocatable    :: smb(:,:)     ![m yr-1] surface mass balance
    real(dp), allocatable    :: bmb(:,:)     ![m yr-1] basal mass balance
  end type

  type mb_class
    type(mb_state_class)    :: now
  end type

  !================other params===========!
  type mdl_param_class
    real(dp)                 :: dtt          ![yr] model time step
    real(dp)                 :: time_init    ![yr] model start time
    real(dp)                 :: time_end     ![yr] model end time
  end type

  !================final==================!
  type model_class
    type(topo_class)        :: tpo
    type(grd_class)         :: grd
    type(dyn_class)         :: dyn
    type(mdl_param_class)   :: par
    type(mb_class)          :: mb
  end type
  !END FROM YELMO ##########################################################################


  !ELSA ************************************************************************************
  ! === Define isochrone type for Yelmo =====
  type elsa_param_class
    ! elsa parameters
    integer                 :: n_layers!              [1] vertical size of isochronal grid
    integer                 :: nx_iso!                [1] size of lateral grid
    integer                 :: ny_iso!                [1] size of lateral grid
    real(dp)                :: dx_iso!                [m] grid box size in x direction
    real(dp)                :: dy_iso!                [m] grid box size in y direction
    integer                 :: init_layers!           [1] number of initialized layers at start of simulation
    integer                 :: grid_factor!           [1] regridding step size, average grid_factor many yelmo boxes in x/y into one
    real(dp)                :: update_factor!         [1] use yelmo physics every ctrl%dtt*update_factor years
    real(dp)                :: layer_resolution!     [yr] create a new isochronal layer every layer_resolution years
    integer                 :: elsa_out!              [yr] output time step
    logical                 :: tracer_on!             switch to include dye tracer
    logical                 :: allow_pos_bmb!         if True, add positive bmb to bottom layer (init layer)
    real(dp)                :: update_dt!             [yr] update timestep for elsa (= model timestep*update_factor)
    real(dp)                :: cutoff!                [m] cutoff value for LIS solver in elsa_update
    real(dp), allocatable   :: layer_list(:)!         [yr] list of layers to add if required
  end type elsa_param_class

  type elsa_state_class
  ! elsa state
    integer               :: top_layer!             [1] the current isochrone at the ice surface
    real(dp), allocatable :: vx_iso(:,:,:)!         [m/a] x velocity on isochronal grid
    real(dp), allocatable :: vy_iso(:,:,:)!         [m/a] y velocity on isochronal grid
    real(dp), allocatable :: dsum_iso(:,:,:)!       [m] elevation over bedrock of each layer
    real(dp), allocatable :: d_iso(:,:,:)!          [1] layer thickness as a fraction of total ice thickness
    real(dp), allocatable :: tracer_iso(:,:,:)!     [1] passive dye tracer
    real(dp), allocatable :: H_ice_iso(:,:)!        [m] ice thickness on isochronal x/y grid
    real(dp), allocatable :: sum_smb(:,:)!          [m] cumulative SMB to be applied at layer_resolution time step
    real(dp), allocatable :: sum_bmb(:,:)!          [m] cumulative BMB to be applied at layer_resolution time step
    real(dp), allocatable :: misc_1(:,:,:)!           [m] miscellaneous/temporary 2d array that can be used for debugging
 end type elsa_state_class

  type elsa_class
    type(elsa_param_class)    :: par
    type(elsa_state_class)    :: now
  end type
  !END YELSA ******************************************************************************

end module elsa_classes
