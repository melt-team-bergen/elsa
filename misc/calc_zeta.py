#quick script to calculate zeta_aa from layer boundaries

import numpy as np

za = np.array([0, 0.1393, 0.2987, 0.4665, 0.6402, 0.8183, 1.0000]) #input your layer boundaries here.
zeta_aa = np.zeros([np.size(za)+1])

zeta_aa[0] = za[0]; zeta_aa[-1] = za[-1]

for ii in range(np.size(zeta_aa) - 2):

  zeta_aa[ii+1] = (za[ii+1] + za[ii])/2

print(zeta_aa)














