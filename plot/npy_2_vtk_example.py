#pvista plotting

import os
import sys
import numpy as np
import pyvista as pv
import scipy.io as sio
#import pyevtk.hl as pyvtk
import matplotlib.pyplot as plt

#os.chdir(os.path.dirname(sys.argv[0]))
#dir = os.path.dirname(os.path.realpath(__file__))

plot = False

res = 2000 #grid resolution
z_factor = 1 #stretch by factor
layers = np.array([1, 2, 3, 50, 70, 90, 110]) #layers to extract for vtk
#layers = np.array([5, 7, 9, 11, 13])
layers = layers -1 #to python indexing

#load data
d_iso = np.load('/work2/rob/git-repos/elsa/output/example/d_iso_out.npy')
dsum_iso = np.load('/work2/rob/git-repos/elsa/output/example/dsum_iso_out.npy')
dsum_iso = np.nan_to_num(dsum_iso, nan=3.14159)

if plot:
  fig, axes = plt.subplots(nrows=2, ncols=3, figsize=(12, 8))
  for ii, ax in enumerate(axes.flat):
      ax.imshow(dsum_iso[:,:,layers[ii]], cmap='viridis')  # You can change the colormap ('viridis' in this case) as needed
      ax.set_title(f'Image {ii+1}')
      ax.axis('off')  # Turn off axis for clarity
  plt.tight_layout()
  plt.show()

#z_bed = np.load('/work2/rob/git-repos/elsa/output/isochrone_project/z_bed_out.npy')
z_bed = np.load('/work2/rob/git-repos/elsa/output/example/z_bed.npy')
z_bed = np.nan_to_num(z_bed, nan=-1000*3.14159)

#create grid
xx = np.arange(start = 0, step = res, stop = res*d_iso.shape[1])
yy = np.arange(start = 0, step = res, stop = res*d_iso.shape[0])
xx, yy = np.meshgrid(xx, yy)

plt.imshow(dsum_iso[:,:,10])

#loop over layers
block = pv.MultiBlock()
for ii in range(layers.shape[0]):
    zz = dsum_iso[:,:,layers[ii]] + z_bed
    zz[zz<-1000] = -1000
    grid = pv.StructuredGrid(xx, yy, zz*z_factor)
    block.append(grid)

merged=block.combine()
merged.save('/work2/rob/git-repos/elsa/plot/plots_out/merged_out.vtk')
#merged.plot()

#save base seperately 
grid = pv.StructuredGrid(xx,yy,z_bed*z_factor)
grid.save('/work2/rob/git-repos/elsa/plot/plots_out/base.vtk')


















