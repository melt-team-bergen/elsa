# configuration script for adding/editing necessary scripts from ELSA to yelmox
# Therese Rieckh
# University of Bergen, 2023



### EDIT PATHS ###
ELSAROOT='/work2/trieckh/git-repos/elsa-yelmo-coupled/elsa'
YELMOROOT='/work2/trieckh/git-repos/elsa-yelmo-coupled/yelmo'
YELMOXROOT='/work2/trieckh/git-repos/elsa-yelmo-coupled/yelmox'
LISROOT='/work2/trieckh/libs/lis'
FORTRANROOT='/usr'
### EDIT PATHS ###


echo "  "
echo "  "
echo "  "
echo ">>> Make sure you have adjusted the ELSAROOT, YELMOROOT, YELMOXROOT, and LISROOT above in the this file (configure_yelmox_elsa.sh) <<<"
echo "  "
echo "  "
echo "  "

echo "Starting confguration of elsa-yelmox"


ELSAY=${ELSAROOT%%/}/model_integration/yelmox_v1.801

echo "Editing paths in elsa_gfortran"
echo "  "
sed -i "s%MYFORTRAN%${FORTRANROOT}%g" $ELSAY/elsa_gfortran
sed -i "s%MYLIS%${LISROOT}%g" $ELSAY/elsa_gfortran
sed -i "s%MYYELMO%${YELMOROOT}%g" $ELSAY/elsa_gfortran
sed -i "s%MYELSA%${ELSAROOT}%g" $ELSAY/elsa_gfortran


echo "Editing paths in elsa.sh"
echo "  "
sed -i "s%MYELSA%${ELSAROOT}%g" $ELSAY/elsa.sh


echo "Editing paths in compile-elsa_yelmox.sh"
echo "  "
sed -i "s%MYELSA%${ELSAROOT}%g" $ELSAY/compile-elsa-yelmox.sh
sed -i "s%MYLIS%${LISROOT}%g" $ELSAY/compile-elsa-yelmox.sh



# copy compile script to elsa folder
echo "Copying compile_yelmox_elsa to elsa dir"
echo "  "
cp $ELSAY/compile-elsa-yelmox.sh $ELSAROOT/compile-elsa-yelmox.sh


# copy elsa_yelmox.f90 script to elsa/run_scripts folder
echo "Copying elsa_yelmox.f90 to elsa/run_scripts/ dir"
echo "  "
cp $ELSAY/elsa_yelmox.f90 $ELSAROOT/run_scripts/elsa_yelmox.f90


# copy over compiler settings
echo "Copying elsa_gfortran compiler settings to yelmox dir"
echo "  "
cp $ELSAY/elsa_gfortran $YELMOXROOT/config/elsa_gfortran


# add executable alias yelmox_elsa
echo "Adding elsa executable alias to runylmox.js file"
echo "  "
cp $YELMOXROOT/config/runylmox.js $YELMOXROOT/runylmox.js # ensure that runylmox.js is in main folder
sed -i  '/"iso"/a"elsa"   : "libyelmox/bin/yelmox_elsa.x",' $YELMOXROOT/runylmox.js


# copy over yelmox Makefile with yelmox_elsa compiling and library linking
echo "Copying Makefile to yelmox dir"
echo "  "
cp $ELSAY/Makefile $YELMOXROOT/config/Makefile


# copy over example namelist file with elsa parameters. Those are:
# ctrl.exp_name
# the entire elsa section (n_layers_init, grid_factor, update_factor, layer_resolution, elsa_out, use_dye_tracer, allow_pos_bmb)
echo "Copying namelist file including elsa parameters to yelmox dir"
echo "  "
cp $ELSAY/yelmo_Greenland_elsa_16km.nml $YELMOXROOT/par/yelmo_Greenland_elsa_16km.nml


# copy over yelmox code with elsa adjustments
echo "Copying yelmox_elsa.f90 to yelmox dir"
echo "  "
cp $ELSAY/yelmox_elsa.f90 $YELMOXROOT/yelmox_elsa.f90


# copy over bash script which 1) configures the yelmox Makefile, 2) compiles ELSA 3) compiles yelmox, 4) starts yelmox testrun
echo "Copying elsa.sh to yelmox dir"
echo "  "
cp $ELSAY/elsa.sh $YELMOXROOT/elsa.sh


echo "Confguration done"
