#compare smb across values for debugging purposes. 

import os
import sys
import numpy as np
import pyvista as pv
import scipy.io as sio
#import pyevtk.hl as pyvtk
import matplotlib.pyplot as plt

#os.chdir(os.path.dirname(sys.argv[0]))
#dir = os.path.dirname(os.path.realpath(__file__))

plot = True

res = 2000 #grid resolution
z_factor = 1 #stretch by factor
layers = np.array([1, 2, 3, 50, 70, 90, 110]) #layers to extract for vtk
#layers = np.array([5, 7, 9, 11, 13])
layers = layers -1 #to python indexing

#load data
example = np.load('/work2/rob/git-repos/elsa/output/example/smb.npy')
isochrone_project = np.load('/work2/rob/git-repos/elsa/output/isochrone_project/15-5_no_constraint8000/smb.npy')

if plot:
  fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 8))

  axes[0].imshow(example, cmap='viridis')  # You can change the colormap ('viridis' in this case) as needed
  #axes[0].set_title(f'Image {ii+1}')
  axes[0].axis('off')  # Turn off axis for clarity

  axes[1].imshow(isochrone_project, cmap='viridis')  # You can change the colormap ('viridis' in this case) as needed
  axes[1].axis('off')  # Turn off axis for clarity

  plt.tight_layout()
  plt.show()



















