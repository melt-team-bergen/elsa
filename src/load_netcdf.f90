!simple subroutine to load a netcdf for use in ELSA using model class
!this may require minor user modification to fit the setup of their particular NetCDF

subroutine load_netcdf(NetCDF_in, model_input, param_file, tt, remove_nans, quiet)

  use elsa_classes
  use netcdf
  use m_npy
  use nml

  implicit none

  character(len=512), intent(in)     :: NetCDF_in, param_file
  type(model_class), intent(inout)  :: model_input
  integer, intent(in)               :: tt !time step of NetCDF file
  logical, intent(in)               :: remove_nans !should nans be removed and replaced with zeros
  logical, intent(in)               :: quiet

  integer(kind=4)                   :: NX, NY, NZ, Nt, ivar, zz, ii, jj
  integer(kind=4)                   :: ncid, status, xtype, ndims, nvars, natts, varid, dimid
  !integer, dimension(5)            :: dimids !to store variable dimension ids (plural)
  character(len=50)                 :: xname, yname, zname, tname, vname
  character(len=2)                  :: xunits
  character(len=20), dimension(7)   :: var_strings
  character(len=20), dimension(4)   :: dim_strings

  !temporary arrays before transposition and inversion
  real(dp), allocatable :: vx(:,:,:)
  real(dp), allocatable :: vy(:,:,:)
  real(dp), allocatable :: dd(:,:,:)
  real(dp), allocatable :: H_ice(:,:)
  real(dp), allocatable :: smb(:,:)
  real(dp), allocatable :: bmb(:,:)
  real(dp), allocatable :: z_bed(:,:)

  !temporary array to get resolution
  real(dp), allocatable :: xx(:)


  !set some variable call names
  call nml_read(param_file,"NetCDF","velocity_x",var_strings(1))
  call nml_read(param_file,"NetCDF","velocity_y",var_strings(2))
  call nml_read(param_file,"NetCDF","H_ice",var_strings(4))
  call nml_read(param_file,"NetCDF","smb",var_strings(5))
  call nml_read(param_file,"NetCDF","bmb",var_strings(6))
  call nml_read(param_file,"NetCDF","z_bed",var_strings(7))
  call nml_read(param_file,"NetCDF","xdim",dim_strings(1))
  call nml_read(param_file,"NetCDF","ydim",dim_strings(2))
  call nml_read(param_file,"NetCDF","zdim",dim_strings(3))
  call nml_read(param_file,"NetCDF","time",dim_strings(4))
  call nml_read(param_file,"NetCDF","xunits",xunits)

  print *, 'trying to read netcdf: ', NetCDF_in

  !open NetCDF
  call check(nf90_open(NetCDF_in, nf90_nowrite, ncid))
  if (.not.(quiet)) print *, ''//achar(27)//'[94m Opened NetCDF ', NetCDF_in, 'with id ', ncid, ' '//achar(27)//'[97m'

  !basic inquire
  call check(nf90_inquire(ncid, ndims, nvars, natts))
  if (.not.(quiet)) print *, ''//achar(27)//'[92m The NetCDF has ', ndims, ' dimensions and ', nvars, ' variables'

  !get size
  call check(nf90_inq_dimid(ncid, dim_strings(1), dimid))
  call check(nf90_inquire_dimension(ncid,dimid,xname,NX))

  call check(nf90_inq_dimid(ncid, dim_strings(2), dimid))
  call check(nf90_inquire_dimension(ncid,dimid,yname,NY))

  call check(nf90_inq_dimid(ncid, dim_strings(3), dimid))
  call check(nf90_inquire_dimension(ncid,dimid,zname,NZ))

  if ( dim_strings(4) .ne. 'NA') then
    call check(nf90_inq_dimid(ncid, dim_strings(4), dimid))
    call check(nf90_inquire_dimension(ncid,dimid,tname,Nt))
  end if

  if (.not.(quiet)) print *, 'Grid size is ', NX, '(x) by ', NY, '(y) with ', NZ, '(z) layers'

  !get dimension information if first time step
  !zeta_aa
  if (tt == 1) then

    !dx
    call check(nf90_inq_varid(ncid, dim_strings(1), varid))
    allocate(xx(NX))
    call check(nf90_get_var(ncid, varid, xx))
    model_input%grd%par%dx = xx(2)-xx(1)
    if (xunits .eq. 'km') then
      model_input%grd%par%dx=model_input%grd%par%dx*1000 !convert to m
    end if

    !dy
    call check(nf90_inq_varid(ncid, dim_strings(2), varid))
    deallocate(xx)
    allocate(xx(NY))
    call check(nf90_get_var(ncid, varid, xx))
    model_input%grd%par%dy = xx(2)-xx(1)
    if (xunits .eq. 'km') then
      model_input%grd%par%dy=model_input%grd%par%dy*1000 !convert to m
    end if

    !dz
    call check(nf90_inq_varid(ncid, dim_strings(3), varid))
    allocate(model_input%grd%par%zeta_aa(NZ))
    call check(nf90_get_var(ncid, varid, model_input%grd%par%zeta_aa))

    if (.not.(quiet)) print *, 'dx: ', model_input%grd%par%dx, ' m'
    if (.not.(quiet)) print *, 'dy: ', model_input%grd%par%dx, ' m'

    if ( dim_strings(4) .ne. 'NA') then
      if (.not.(quiet)) print *, 'There are ', Nt, ' time steps'
    else
      if (.not.(quiet)) print *, 'No time steps included'
    end if
  end if

  !allocate arrays for data
  allocate(H_ice(NX,NY))
  allocate(smb(NX,NY))
  allocate(bmb(NX,NY))
  allocate(z_bed(NX,NY))
  allocate(vx(NX,NY,NZ))
  allocate(vy(NX,NY,NZ))

  if (allocated(model_input%tpo%now%H_ice))     deallocate(model_input%tpo%now%H_ice)
  if (allocated(model_input%tpo%now%z_bed))     deallocate(model_input%tpo%now%z_bed)
  if (allocated(model_input%mb%now%smb))        deallocate(model_input%mb%now%smb)
  if (allocated(model_input%mb%now%bmb))        deallocate(model_input%mb%now%bmb)
  if (allocated(model_input%dyn%now%ux))        deallocate(model_input%dyn%now%ux)
  if (allocated(model_input%dyn%now%uy))        deallocate(model_input%dyn%now%uy)

  allocate(model_input%tpo%now%H_ice(NY,NX))
  allocate(model_input%tpo%now%z_bed(NY,NX))
  allocate(model_input%mb%now%smb(NY,NX))
  allocate(model_input%mb%now%bmb(NY,NX))
  allocate(model_input%dyn%now%ux(NY,NX,NZ))
  allocate(model_input%dyn%now%uy(NY,NX,NZ))

  !start ascribing data
  model_input%grd%par%nx = NX
  model_input%grd%par%ny = NY
  model_input%grd%par%nz_aa = NZ

  !loop over variables
  !if (.not.(quiet)) print *, 'Variables in NetCDF are:'
  !do ivar = 1, nvars
  !  dimids = 999
  !  call check(nf90_inquire_variable(ncid, ivar, vname, xtype, ndims, dimids))
  !  if (.not.(quiet)) print *, vname, '(dims=', ndims, ')'
  !  if (.not.(quiet)) print *, dimids
  !end do

  !assign arrays based on time

  if ( dim_strings(4) .ne. 'NA') then  !if time

    if (.not.(quiet)) print *, 'Assigning vx'
    call check(nf90_inq_varid(ncid, trim(var_strings(1)), varid))
    call check(nf90_get_var(ncid, varid, vx, start = [1,1,1,tt]))
    if (.not.(quiet)) print *, 'Assigning vy'
    call check(nf90_inq_varid(ncid, trim(var_strings(2)), varid))
    call check(nf90_get_var(ncid, varid, vy, start = [1,1,1,tt]))
    if (.not.(quiet)) print *, 'Assigning H_ice '
    call check(nf90_inq_varid(ncid, trim(var_strings(4)), varid))
    call check(nf90_get_var(ncid, varid, H_ice, start = [1,1,tt])) !, map = [1, 2, 3]))
    if (.not.(quiet)) print *, 'Assigning smb'
    call check(nf90_inq_varid(ncid, trim(var_strings(5)), varid))
    call check(nf90_get_var(ncid, varid, smb, start = [1,1,tt])) !, map = [1, 2, 3]))
    if ( var_strings(6) .ne. 'NA') then
      if (.not.(quiet)) print *, 'Assigning bmb'
      call check(nf90_inq_varid(ncid, trim(var_strings(6)), varid))
      call check(nf90_get_var(ncid, varid, bmb, start = [1,1,tt])) !, map = [1, 2, 3]))
    end if
    if (.not.(quiet)) print *, 'Assigning z_bed as final time step'//achar(27)//'[97m'
    call check(nf90_inq_varid(ncid, trim(var_strings(7)), varid))
    call check(nf90_get_var(ncid, varid, z_bed, start = [1,1,Nt])) !, map = [1, 2, 3]))

  else !if no time

    if (.not.(quiet)) print *, 'Assigning vx'
    call check(nf90_inq_varid(ncid, trim(var_strings(1)), varid))
    call check(nf90_get_var(ncid, varid, vx))
    if (.not.(quiet)) print *, 'Assigning vy'
    call check(nf90_inq_varid(ncid, trim(var_strings(2)), varid))
    call check(nf90_get_var(ncid, varid, vy))
    if (.not.(quiet)) print *, 'Assigning H_ice '
    call check(nf90_inq_varid(ncid, trim(var_strings(4)), varid))
    call check(nf90_get_var(ncid, varid, H_ice)) !, map = [1, 2, 3]))
    if (.not.(quiet)) print *, 'Assigning smb'
    call check(nf90_inq_varid(ncid, trim(var_strings(5)), varid))
    call check(nf90_get_var(ncid, varid, smb)) !, map = [1, 2, 3]))
    if ( var_strings(6) .ne. 'NA') then
      print *, var_strings(6)
      if (.not.(quiet)) print *, 'Assigning bmb'
      call check(nf90_inq_varid(ncid, trim(var_strings(6)), varid))
      call check(nf90_get_var(ncid, varid, bmb, start = [1,1,tt])) !, map = [1, 2, 3]))
    end if
    if (.not.(quiet)) print *, 'Assigning z_bed '
    call check(nf90_inq_varid(ncid, trim(var_strings(7)), varid))
    call check(nf90_get_var(ncid, varid, z_bed)) !, map = [1, 2, 3]))

  end if

  !remove nans from velocity and H_ice data
  if (remove_nans) then
    do ii = 1, NX
      do jj = 1, NY
        if (isnan(H_ice(ii, jj))) then
          H_ice(ii, jj) = 0
        end if

        do zz = 1, NZ
          if (isnan(vx(ii,jj,zz))) then
            vx(ii, jj, zz) = 0
          end if
          if (isnan(vy(ii,jj,zz))) then
            vy(ii, jj, zz) = 0
          end if
        end do
      end do
    end do
  end if

  !rotate -90 (invert each row then transpose)
  model_input%tpo%now%H_ice = transpose(H_ice(:, NY:0:-1))
  model_input%mb%now%smb = transpose(smb(:, NY:0:-1))
  model_input%mb%now%bmb = transpose(bmb(:, NY:0:-1))
  model_input%tpo%now%z_bed = transpose(z_bed(:, NY:0:-1))
  do zz = 1, NZ
    model_input%dyn%now%ux(:,:,zz) = transpose(vx(:, NY:0:-1, zz))
    model_input%dyn%now%uy(:,:,zz) = transpose(vy(:, NY:0:-1, zz))
  end do

  !deallocate temporary arrays
  deallocate(H_ice)
  deallocate(vx)
  deallocate(vy)
  deallocate(smb)
  deallocate(bmb)
  deallocate(z_bed)

  !close
  call check(nf90_close(ncid))

  !printing out loaded arrays
  !call save_npy("/work2/rob/git-repos/elsa/output" // "/z_bed.npy", model_input%tpo%now%z_bed(:,:))
  !call save_npy("/work2/rob/git-repos/elsa/output" // "/H_ice.npy", model_input%tpo%now%H_ice(:,:))
  !call save_npy("/work2/rob/git-repos/elsa/output" // "/ux.npy", model_input%dyn%now%ux(:,:,:))
  !call save_npy("/work2/rob/git-repos/elsa/output" // "/uy.npy", model_input%dyn%now%uy(:,:,:))
  !call save_npy("/work2/rob/git-repos/elsa/output" // "/smb.npy", model_input%mb%now%smb(:,:))

contains

  subroutine check(stat)

    implicit none

    integer, intent(in) :: stat

    if (stat /= NF90_NOERR) then
      print *, "NetCDF not opened successfully"
      stop
    !else
    !  print *, "NetCDF file opened successfully"
    end if
  end subroutine check

  end subroutine load_netcdf
