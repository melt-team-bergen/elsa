#!/bin/bash

# NOTE adjust your run (output dir name) and nml inout files here
run=GRL_16km_elsa-200a-layerfile
nml=GRL_16km_example.nml

# compile ELSA
ELSAROOT=/work2/trieckh/git-repos/elsa-yelmo-coupled/elsa
bash $ELSAROOT/compile-elsa-yelmox.sh

# configure and compile yelmox code
python3 config.py config/elsa_gfortran

make clean
make yelmox_elsa

# start simulation
./runylmox -r -e elsa -o output/$run -n par/$nml
