#pvista plotting

import os
import sys
import numpy as np
import pyvista as pv
import scipy.io as sio
#import pyevtk.hl as pyvtk
import matplotlib.pyplot as plt

#os.chdir(os.path.dirname(sys.argv[0]))
#dir = os.path.dirname(os.path.realpath(__file__))

output_dir = '/work2/rob/git-repos/elsa/output/runs/Gullhorgabu/'
runname = 'Gullhorgabu'
save_dir = '/work2/rob/git-repos/elsa/plot/plots_out/'

plot = False

res = 8000 #grid resolution
surf_layer_val = 60 #layer value for surface
base_layer_val = -1 #layer value for base


layers = np.array([10, 20, 30, 40, 49]) #layers to extract for vtk
layers = layers -1 #to python indexing

#load data
d_iso = np.load(output_dir + 'd_iso_out.npy')
dsum_iso = np.load(output_dir + 'dsum_iso_out.npy')
dsum_iso = np.nan_to_num(dsum_iso, nan=-3141.59)
print(np.shape(dsum_iso))

z_bed = sio.loadmat(output_dir + '../zbed_out.mat')
z_bed = z_bed['zbed_out']
z_bed = np.nan_to_num(z_bed, nan=-1000*3.14159)
#z_bed = np.flipud(z_bed)

z_sur = sio.loadmat(output_dir + '../zsur_out.mat')
z_sur = z_sur['zsur_out']
z_sur = np.nan_to_num(z_sur, nan=-1000*3.14159)
#z_sur = np.flipud(z_sur)

if plot:
  fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12, 8))
  for ii, ax in enumerate(axes.flat):
      ax.imshow(dsum_iso[:,:,layers[ii]], cmap='viridis')  # You can change the colormap ('viridis' in this case) as needed
      ax.set_title(f'Image {ii+1}')
      ax.axis('off')  # Turn off axis for clarity
  plt.tight_layout()
  plt.show()
  sys.exit()

#create grid
xx = np.arange(start = 0, step = res, stop = res*d_iso.shape[1])
yy = np.arange(start = 0, step = res, stop = res*d_iso.shape[0])
xx, yy = np.meshgrid(xx, yy)

### loop over layers ###
block = pv.MultiBlock()
for ii in range(layers.shape[0]):
    
    #create grid
    zz = dsum_iso[:,:,layers[ii]]
    zz = np.flipud(zz)
    zz = zz + z_bed
    zz[zz<-1000] = -1000
    grid = pv.StructuredGrid(xx, yy, zz)

    #create data to add
    data = zz[:-1,:-1]
    gx, gy = np.gradient(data) 
    data = np.sqrt(np.power(gx, 2) + np.power(gy, 2))
    layer = np.zeros(grid.n_cells)
    layer[:] = layers[ii]

    zz_data = np.reshape(zz[:-1,:-1], newshape=(xx.shape[0]-1)*(xx.shape[1]-1), order='F') 
    data = np.reshape(data, newshape=(xx.shape[0]-1)*(xx.shape[1]-1), order='F') 

    grid.cell_data['gradient'] = data
    grid.cell_data['z'] = zz_data
    grid.cell_data['layer'] = layer

    #add grid
    block.append(grid)

### add in surface ###
zz = z_sur
zz[zz<-1000] = -1000
#zz = np.flipud(zz)

grid = pv.StructuredGrid(xx, yy, zz)

#create data to add
data = zz[:-1,:-1]
gx, gy = np.gradient(data) 
data = np.sqrt(np.power(gx, 2) + np.power(gy, 2))
layer = np.zeros(grid.n_cells)
layer[:] = surf_layer_val

zz_data = np.reshape(zz[:-1,:-1], newshape=(xx.shape[0]-1)*(xx.shape[1]-1), order='F') 
data = np.reshape(data, newshape=(xx.shape[0]-1)*(xx.shape[1]-1), order='F') 

grid.cell_data['gradient'] = data
grid.cell_data['z'] = zz_data
grid.cell_data['layer'] = layer
block.append(grid)

### add in bed ###
zz = z_bed
zz[zz<-1000] = -1000
#zz = np.flipud(zz)

grid = pv.StructuredGrid(xx, yy, zz)

#create data to add
data = zz[:-1,:-1]
gx, gy = np.gradient(data) 
data = np.sqrt(np.power(gx, 2) + np.power(gy, 2))
layer = np.zeros(grid.n_cells)
layer[:] = base_layer_val

zz_data = np.reshape(zz[:-1,:-1], newshape=(xx.shape[0]-1)*(xx.shape[1]-1), order='F') 
data = np.reshape(data, newshape=(xx.shape[0]-1)*(xx.shape[1]-1), order='F') 

grid.cell_data['gradient'] = data
grid.cell_data['z'] = zz_data
grid.cell_data['layer'] = layer
block.append(grid)

### merge and save ###
merged=block.combine()
merged.save(save_dir + runname + '_merged_out.vtk')
print('saved output to ' + save_dir + runname + '_merged_out.vtk' + ' for layers ', (layers + 1))
#merged.plot()

#save base seperately 
#grid = pv.StructuredGrid(xx,yy,z_bed*z_factor)
#grid.save('/work2/rob/git-repos/elsa/plot/plots_out/base.vtk')


















