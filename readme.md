# elsa

elsa (englacial layer simulation architecture) is a Fortran 2003 program to model isochrone position within ice sheets. elsa is ready to be linked to NetCDF inputs (running offline; elsa-0) or directly to binary outputs from your ice sheet model (see folder model_integration). elsa is developed from [Born and Robinson (2021)](https://tc.copernicus.org/articles/15/4539/2021/), *Modeling the Greenland englacial stratigraphy* with further forthcoming publications from the MELT team.

## Installation of elsa-0

elsa-0 is presently only tested for Debian (Ubuntu) Linux*. To install elsa-0 you need to copy compile-elsa-0.sh, edit it so that the paths are compatible with your machine, and then run it: 

```bash
bash compile-elsa-0.sh
```

*Two* external packages (outlined below) are required, NetCDF and Lis. MPI is required if you wish to use elsa-0 in parallel, but this functionality is presently under development. Comments in compile-elsa-0.sh provide further information on how to structure path locations.

<small>*but complexity is low and Lis and NetCDF work on other platforms so porting shouldn't be difficult</small>


**NetCDF** 

This library is required to import and export NetCDF files into Fortran. NetCDF is not deeply imbedded, so if you wish to eschew it entirely you can, just remove all references to it from the code. NetCDF for Fortran90 can be installed with:

```bash
sudo apt-get install libnetcdff-dev
```
Further information at [packages.debian.org/sid/libnetcdff-dev](https://packages.debian.org/sid/libnetcdff-dev). 

**Lis**

Short for Library of Iterative Solvers for Linear Systems. This library is required to numerically solve the advection equations within elsa. Further information is available at [ssisc.org/lis/index.en.html](https://www.ssisc.org/lis/index.en.html). Follow the [installation guide](https://www.ssisc.org/lis/lis-ug-en.pdf) carefully and make sure to use `--enable-f90` and `--enable-fortran` flags for Fortran compatability.

**MPI**

Required to parellelise Lis (under development). Further install information [here](https://edu.itp.phys.ethz.ch/hs12/programming_techniques/openmpi.pdf).

## Usage

The basic command for elsa-0 is:

```bash
./elsa0 --input-file "input.nml"
```

where `"input.nml"` is a namelist file containing paths to other input files, model input metadata, and elsa0 parameters. If you run:

```bash
./elsa0 
```
with no tags the default input will be `"example.nml"` which will use constant NetCDF input from a simple ISSM simulation of the Greenland Ice Sheet. This runs for 500 years, takes about 2 minutes on an i7 laptop, and is a good test to see if elsa0 is up and running succesfully (just don't expect useful output). 

**What elsa0 needs to run**

<!-- ![add a description here when you input an image](/msc/img/haccs.png "haccs") -->


**NetCDF**

elsa-0 can take model output NetCDF files as inputs. You can use [Ncview](https://cirrus.ucsd.edu/ncview/) to get an idea of the structure of the example input input/output.nc from a simple ISSM simulation. And see above for the variables required by elsa0. You can redirect NetCDF variable names in the .nml input file where you should also specify the x and y resolution (res_x and res_y) and number of layers (num_layers). 

**Binaries**

We are constructing a binary interface for [yelmo](https://github.com/palma-ice/yelmo). We welcome your contributions for interfaces with other ice sheet models (please see Contributing, below).

**zeta_aa**

This goes into the zeta_aa.txt file and tells elsa how to interpret the layers in your model input. Points are defined as the top and bottom (0 and 1) and as the midpoint of each layer. 

![zeta_aa](./misc/img/zeta_aa.png)

## Using elsa coupled to an ice sheet model

elsa is currently available coupled to the ice sheet model Yelmo. The folder model_integration contains a separate README with all information necessary to install Yelmo and install the coupled setup.


## Contributing

We welcome contributions for subroutines linking elsa to additional ice sheet models. Please follow the binary interface for yelmo as an initial example and get in touch if you would like to discuss this further.

## MELT team 

We are Therese Rieckh (therese.rieckh@uib.no), Robert Law (robert.law@uib.no) (Postdoctoral Research Fellows), and Andreas Born (andreas.born@uib.no) (Professor), all at the University of Bergen. We welcome your feedback and comments.

## License

See license information
