"""
Description: interpolation script for Operation IceBridge data

Author: Therese Rieckh | therese.rieckh@uib.no
Based on the interpolation script by Luisa Hübner
"""


import netCDF4 as nc
import numpy as np
import scipy.interpolate as sp
import xarray as xr
import sys

#TODO change units of the interpolated data

#where to save result
output_path = '/home/rese/from_joe/v2/interp_icebridge_16km_v2.nc'

#load target grid on which to interpolate
target_grid_path = '/home/rese/from_joe/v2/GRL-16KM_TOPO-M17.nc'
target_grid = nc.Dataset(target_grid_path, mode = 'r')

#load source data to be interpolated
source_data_path = '/home/rese/from_joe/v2/Greenland_isochrone_grid_v2.nc'
source_data = nc.Dataset(source_data_path)

#specify list of which variables to interpolate
input_vars = ['depth', 'std']#['d2m', 'tp', 'strd', 'ssrd', 't2m']

age_dim = True #True if variables that should be interpolated have a age dimension

#choose which agestep to interpolate
choose_step = -1 #-1 to interpolate all steps

#coordinate variable names
target_xname = 'x2D'
target_yname = 'y2D'

source_xname = 'x'
source_yname = 'y'

#output format
output_format = "NETCDF4_CLASSIC"


def create_output_file():
    """
    Function that creates a NetCDF output file, with the dimensions of
    the target grid and an optional age dimension. The target grid
    coordinate variables are copied. The variables that should be
    interpolated are initialized with an optional age dimension and
    the x and y dimensions of the target coordinates. Variable
    attributes are copied from the source file. Returns: output file,
    number of age steps
    """
    #create new empty file for the interpolated data
    result = nc.Dataset(output_path, mode = "w", format = output_format)

    #check if age dimension is needed, if so, create age dimension and variable
    if np.size(list(target_grid.variables[target_xname].dimensions)[:]) != 2:
        sys.exit('Error: The coordinates of the target script have an unexpected number of dimensions. The number of dimensions should be 2.')

    if np.logical_and(age_dim, choose_step == -1):
        result.createDimension('age', None)
        result.createVariable('age', 'i4', ('age'))
        result.variables['age'][:] = source_data.variables['age'][:]*1000
        setattr(result.variables['age'], 'units', 'a before present')

        age_steps = len(source_data.variables['age'])

        dims = np.array(['age',list(target_grid.variables[target_xname].dimensions)[0], list(target_grid.variables[target_xname].dimensions)[1]]) # create list of Dimensions the interpolated variable should have. The dimensions have to be defined in the same order as for the target coordinate variables.
    else:
        age_steps = 1
        dims = list(target_grid.variables[target_xname].dimensions)[:]

    #copy the dimensions of the target grid into the new file
    for dim in list(target_grid.dimensions.keys()):
        result.createDimension(dim, len(target_grid.dimensions[dim]))

    #copy the variables of the target grid into the new file
    for var in [target_xname, target_yname]:
        result.createVariable(var, 'f8', (target_grid.variables[var].dimensions))
        result.variables[var][:] = target_grid.variables[var][:]
        for attr in target_grid.variables[var].ncattrs():
            setattr(result.variables[var], attr, getattr(target_grid.variables[var], attr))

    #add variable that should be interpolated to the output file and copy its attributes from the source file
    for var in input_vars:
        result.createVariable(var, 'f8', dims)
        for attr in source_data.variables[var].ncattrs():
            if np.logical_and(np.logical_and(attr != '_FillValue', attr != 'scale_factor'), attr != 'add_offset'): #scale factor and offset should not be copied because they lead to incorrect data when using Bessi, we also dont want the Fill Value
                setattr(result.variables[var], attr, getattr(source_data.variables[var], attr))
    return result, age_steps


def create_1D_source_coords():
    """
    create array with x and y koordinates of source data with shape
    (#datapoints,2) and crop off data that lies outside of the region
    of the target grid. Returns array of source coordinates, array of
    selected indices for the x coordinates and array of selected
    indices for the y coordinates
    """
    # target_x_style_change = 0 # marks if the definition of the x coordinates has been changed
    target_x = target_grid.variables[target_xname][:]
    # if np.max(target_x) > 180:
    #     target_x[target_x > 180] = target_x[target_x > 180] - 360
    #     target_x_style_change = 1
    #min and max values for x and y
    xmin = np.min(target_x) -2 #keep in mind that the data for greenland overlaps with the Meridian
    xmax = np.max(target_x) +2
    ymin = np.min(target_grid.variables[target_yname][:]) -2 #+/- 2 in order to not cut the data off too close
    ymax = np.max(target_grid.variables[target_yname][:]) +2

    x = source_data.variables[source_xname][:]/1000.
    # if np.max(x) > 180:
    #     x[x > 180] = x[x > 180] - 360
    y = source_data.variables[source_yname][:]/1000.

    #Masks to identify which data points lie within the area of the target grid
    selected_indices_x = np.logical_and(x >= xmin, x <= xmax)
    selected_indices_y = np.logical_and(y >= ymin, y <= ymax)
    source_coordinates = np.zeros((np.sum(selected_indices_x) * np.sum(selected_indices_y), 2))

    X,Y = np.meshgrid(x[selected_indices_x], y[selected_indices_y])
    # if target_x_style_change:
    #     X[X < 0] = X[X < 0] + 360 # makes sure the source x coordinates and the target x coordinates are defined in the same way ((0,360) or (-180,180))

    source_coordinates[:,0] = np.ndarray.flatten(X)
    source_coordinates[:,1] = np.ndarray.flatten(Y)

    return source_coordinates, selected_indices_x, selected_indices_y


def create_1D_target_coords(result):
    """
    create array with x and y coordinates of the target grid with
    shape (#gridpoints,2). input: output NetCDF file; returns: array
    of target coordinates, number of grid points in x direction,
    number of grid points in y direction
    """
    nx = np.shape(result.variables[target_xname][:])[0]
    ny = np.shape(result.variables[target_xname][:])[1]

    target_coordinates = np.zeros((nx * ny, 2))
    target_coordinates[:,0] = np.ndarray.flatten(result.variables[target_xname][:])
    target_coordinates[:,1] = np.ndarray.flatten(result.variables[target_yname][:])

    return target_coordinates, nx, ny


def interp(var, age_step, source_data_values, result):
    """
    interpolate the data onto the target grid, check for and eliminate
    missing values and save result in the output file. Input:
    variable, agestep, source_data, output file
    """
    #interpolate with scipy.interpolate.griddata
    data_interp = sp.griddata(source_coordinates, source_data_values, target_coordinates, method = 'linear', fill_value = 10e+30) #fill_value is set very large so missing points can be easily identified

    #reshape the interpolated data into a 2D array
    data_interp_2D = np.reshape(data_interp, (np.shape(result.variables[target_xname][:])[0], np.shape(result.variables[target_xname][:])[1]))

    #check for missing values and approximate them by taking the mean of the nearest neighbours that are not missing
    missing = np.argwhere(data_interp_2D == 10e+30)
    for i in np.arange(0, np.shape(missing)[0], 1):
        missing_neighbours = data_interp_2D[missing[i,0]-1:missing[i,0]+2:1, missing[i,1]-1:missing[i,1]+2:1]
        data_interp_2D[missing[i,0],missing[i,1]] = np.mean(missing_neighbours[missing_neighbours != 10e+30])

    if np.sum(data_interp_2D == 10e+30) != 0: #if there are still missing values after this, something went wrong
        print('Warning: Missing values in interpolation of ', var, ' at agestep: ', age_step, ', that could not be fixed.')

    #save the interpolated data in the output file
    if np.logical_and(age_dim, choose_step == -1):
        result.variables[var][age_step,:,:] = data_interp_2D
    else:
        result.variables[var][:] = data_interp_2D



result, age_steps = create_output_file()
source_coordinates, selected_indices_x, selected_indices_y = create_1D_source_coords()
target_coordinates, nx, ny = create_1D_target_coords(result)

#interpolate the data for each age step for each specified variable
if age_dim:
    if choose_step == -1:
        for var in input_vars:
            for age_step in np.arange(0, age_steps, 1):
                print(var,' agestep: ', age_step)
                source_data_values = np.ndarray.flatten(source_data.variables[var][:][age_step,selected_indices_y,:][:,selected_indices_x])
                interp(var, age_step, source_data_values, result)
    else:
        for var in input_vars:
            print(var,' agestep: ', choose_step)
            source_data_values = np.ndarray.flatten(source_data.variables[var][:][choose_step,selected_indices_y,:][:,selected_indices_x])
            interp(var, 0, source_data_values, result)
else:
    for var in input_vars:
        print(var)
        source_data_values = np.ndarray.flatten(source_data.variables[var][:][selected_indices_y,:][:,selected_indices_x])
        interp(var, 0, source_data_values, result)


print('Interpolation complete')
print('nx = ', nx)
print('ny = ', ny)


