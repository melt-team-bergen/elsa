#ELSA compilation script
#MELT team
#University of Bergen, 2023


echo "Compiling elsa-yelmox"


ELSAROOT=/work2/trieckh/git-repos/elsa-yelmo-coupled/elsa
ELSASRC=${ELSAROOT%%/}/src

LISROOT=/work2/trieckh/libs/lis
LISINC=${LISROOT%%/}/include
LISLIB=${LISROOT%%/}/lib


FC=gfortran #the compiler to use, only tested for gfortran

NCF=-I/usr/include #the location of netcdf.mod with the '-I' flag for 'include'. This can be installed with 'apt-get install libnetcdff-dev'.
libNCF="-lnetcdff -lnetcdf" #NETCDF_DIR still needs to point to the correct path (e.g. export NETCDF_DIR=/lib/x86_64-linux-gnu/libnetcdf.so in your .bashrc)

lis=-I/$LISINC #location of the lis library header lisf.h. Follow the docs carefully and make sure to use --enable-f90 and --enable-fortran flags.
liblis="-L/$LISLIB -llis"


yelmox_path=$(pwd)

cd $ELSASRC
echo "removing all .mod, .o, and .a files"
rm -f *.mod *.o *.a

$FC -cpp -g \
        elsa_classes.f90 npy.f90 nml.f90 load_netcdf.f90 f90getopt.F90 load_input_data.f90 elsa_isochrones.f90 \
	../run_scripts/elsa_yelmox.f90 -c $NCF $lis $liblis $libNCF #-g flag can be included for debugging but is not essential

echo "DONE, compiled scripts"

ar rc ../lib/libelsa.a elsa_yelmox.o elsa_classes.o elsa_isochrones.o npy.o nml.o load_netcdf.o f90getopt.o load_input_data.o
echo " "
echo " DONE, libelsa.a is ready."
echo " "

if [ -f ../lib/libelsa.a ]; then
  echo "elsa-yelmox compiled and linked"
else
  echo "compiling error" #something went wrong but you probably have an error for that already
fi

cd $yelmox_path





