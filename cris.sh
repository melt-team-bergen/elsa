#script to compile (c) and run (r) elsa0

run_name="isochrone_project"
nml="15-5_gt_6_steady_state.nml"

bash compile-elsa-0_melt.sh
if [ -f ./bin/elsa0 ]; then

  if [ ! -d "./output/$run_name" ]; then
    echo "creating output directory: ./output/$run_name/"
    echo " "
    mkdir ./output/$run_name/
  fi

  echo "copying .nml file to output" 
  echo " "
  cp input/$run_name/$nml output/$run_name/$nml

  echo "running elsa" #obviously may erroneously be running an old compile 
  echo " "

  #touch file for run status
  if [ -f ./run_complete ]; then
    rm ./output/$run_name/run_complete
  fi
  touch ./output/$run_name/run_ongoing
  ./bin/elsa0 --input-file "input/$run_name/$nml" #"input/example/GRL_iso-10a.nml"
  rm ./output/$run_name/run_ongoing
  touch ./output/$run_name/run_complete

else
  echo "not running elsa" #oh no
fi

