!This is the main base-case program for ELSA. It takes the input data as NetCDF,
!converts it into a form readable by the ELSA subroutines, passes it
!through the ELSA subroutines, and then constructs an output.

!robert.law@uib.no, MELT team, University of Bergen, 2023
!therese.rieckh@uib.no, MELT team, University of Bergen, 2024

program elsa0

  !load modules
  use elsa_classes
  !use system_clock
  use f90getopt
  use netcdf
  use m_npy
  use nml
  use elsa_isochrones
  use load_input_data


  implicit none

  type(model_class)         :: model_input
  type(elsa_class)          :: elsa_layers
  integer             :: time, nn, tt       ![yr] model time, [] time step, [] time slice for NetCDF input
  integer             :: imax, jmax         !size of incoming file/binary from model 
  integer             :: begin, end, rate   !for clock
  character(len=512)  :: NetCDF_in, nml_in, zeta_file, transient, layer_file
  logical             :: remove_nans        !should nans be removed from incoming netcdf file
  real(dp), allocatable   :: layer_recorder(:,:,:)    !record output of layers for debugging purposes 

  character(len=512) :: current_time, run_name, run_dir, exp_name, out_file !simple elsa0 things
  logical :: dir_exist

  !start clock
  !call system_clock(begin, rate)

  !read command arguments to get nml_in
  call read_arguments(nml_in)

  call nml_read(nml_in,"elsa","exp_name", exp_name) !project name, set as NA to use default

  !set a few inputs
  call nml_read(nml_in,"NetCDF","NetCDF_in",NetCDF_in)
  call nml_replace(NetCDF_in,"{exp_name}",trim(exp_name))
  call nml_read(nml_in,"NetCDF","remove_nans",remove_nans)
  call nml_read(nml_in,"model","zeta_file",zeta_file)
  call nml_read(nml_in,"elsa","layer_file",layer_file)
  call nml_replace(layer_file,"{exp_name}",trim(exp_name))
  print *, 'zeta file: ', zeta_file
  print *, 'layer file: ', layer_file

  !load netcdf to model class
  tt = 1
  call load_netcdf(NetCDF_in, model_input, nml_in, tt, remove_nans, .false.)

  !load some parameters
  call nml_read(nml_in,"model","timestep",model_input%par%dtt)
  call nml_read(nml_in,"model","time_init",model_input%par%time_init)
  call nml_read(nml_in,"model","time_end",model_input%par%time_end)
  call nml_read(nml_in,"model","num_layers",model_input%grd%par%nz_aa)
  call nml_read(nml_in,"NetCDF","time",transient) !is the model transient. If transient set as 'NA' then steady state
  call nml_read(nml_in,"elsa","run_name", run_name) !run name, set as NA to use defaults

  call load_zeta(model_input, zeta_file) !sets zeta values (overwrites if zeta_file set as 'NA')'

  !check output dir
  if (exp_name .ne. "NA" .and. run_name == "NA") run_dir = "output/" // trim(exp_name)
  if (exp_name .ne. "NA" .and. run_name .ne. "NA") run_dir = "output/" // trim(exp_name) // "/" // trim(run_name)
  if (exp_name == "NA" .and. run_name == "NA") run_dir = "output/"
  inquire(file=trim(run_dir), exist=dir_exist)
  if (.not. dir_exist) then
    print *, "Error, output directory doesn't exist, please create it"
    stop
  end if

  !set dimensions - may require switching if input is mashed up. This is a result of 
  !differences in array storage between c (which NetCDF uses) and Fortran.
  imax = model_input%grd%par%ny
  jmax = model_input%grd%par%nx

  print *, 'saving to ', trim(run_dir) // "/smb.npy"
  call save_npy(trim(run_dir) // "/smb.npy", model_input%mb%now%smb(:,:))


  !initialise elsa
  call elsa_init(elsa_layers, nml_in, model_input%par%time_init, &
    model_input%par%time_end, model_input%par%dtt, imax, jmax, &
    model_input%grd%par%dx, model_input%grd%par%dy, model_input%tpo%now%H_ice, layer_file)

  !adding code to record bottom layer for debugging concerns
  allocate(layer_recorder(elsa_layers%par%nx_iso, elsa_layers%par%ny_iso, &
  ceiling((real(model_input%par%time_end-model_input%par%time_init))/real(model_input%par%dtt))))


  print *, ''//achar(27)//'[93m beginning main loop '//achar(27)//'[97m'
  !set up main loop
  do nn = 0, ceiling((real(model_input%par%time_end-model_input%par%time_init))/real(model_input%par%dtt))

    !calculate and print time
    time = model_input%par%time_init + nn*model_input%par%dtt
    print *, 'time step: ', nn, '. time: ', time

    !if model time matches elsa time
    if ((time .ne. model_input%par%time_init) .and. &
      its_time(real(time-model_input%par%time_init), real(elsa_layers%par%update_dt), real(model_input%par%dtt))) &
      then

      !update model input. Change value of NetCDF_in to update geometry/velocity
      print *, 'loading NetCDF step ', tt
      if (tt .ne. 1 .AND. transient .ne. "NA") call load_netcdf(NetCDF_in, model_input, nml_in, tt, remove_nans, .true.)
      tt = tt + 1 !update time going into load_netcdf

      !update surface and basal mass balance
      print *, 'updating surface and basal mass balance'
      !call elsa_sum_smb(elsa_layers, elsa_layers%par%update_dt, imax, jmax, &
      !  model_input%mb%now%smb)

      call elsa_add_smb(elsa_layers, elsa_layers%par%update_dt, imax, jmax, &
        model_input%mb%now%smb)

      call elsa_add_bmb(elsa_layers, elsa_layers%par%update_dt, imax, jmax, &
        model_input%mb%now%bmb)


      !and if model time matches isochrone resolution time or list then add new isochrone
      if ( any(elsa_layers%par%layer_list == time) ) then
         call elsa_add_layer(elsa_layers)
         print *, 'new layer added. top_layer is: ', elsa_layers%now%top_layer
         print *, 'number of layers in file: ', elsa_layers%par%n_layers
     end if

      !advect isochrones
      print *, 'advecting isochrones'
      call elsa_update(model_input, elsa_layers, elsa_layers%par%update_dt)

      !record data
      print *, 'recording data'
      layer_recorder(:,:,nn)=elsa_layers%now%d_iso(:,:,1)

    end if
  end do
  !========================

  !save outputs
  write(*,'(A,/,A)') ' ', ''//achar(27)//'[35m simulation complete, saving output '//achar(27)//'[97m', ' '

  call save_npy(trim(run_dir) // "/diso.npy", elsa_layers%now%d_iso(:,:,:))
  call save_npy(trim(run_dir) // "/vx.npy", elsa_layers%now%vx_iso(:,:,:))
  call save_npy(trim(run_dir) // "/vy.npy", elsa_layers%now%vy_iso(:,:,:))
  call save_npy(trim(run_dir) // "/hice.npy", elsa_layers%now%H_ice_iso(:,:))
  call save_npy(trim(run_dir) // "/dsum.npy", elsa_layers%now%dsum_iso(:,:,:))
  call save_npy(trim(run_dir) // "/misc_1.npy", elsa_layers%now%misc_1(:,:,:))
  call save_npy(trim(run_dir) // "/layer_recorder.npy", layer_recorder(:,:,:))
  call save_npy(trim(run_dir) // "/z_bed.npy", model_input%tpo%now%z_bed(:,:))

  print *, 'total number of layers: ', elsa_layers%par%n_layers
  !call system_clock(end)
  !print *, 'runtime: ' real(end-begin)/real(rate)
  !print date and time
  call date_and_time(date=current_time)
  write(*, '(a, a)') 'In the shadows of time: ', trim(current_time)
  call date_and_time(time=current_time)
  write(*, '(a, a)') 'In the depths of time: ', trim(current_time)

end program elsa0








