"""
Description: basic tools for handling ELSA output

Author: Therese Rieckh | therese.rieckh@uib.no
Based on scripts by Andreas Born | andreas.born@uib.no
"""


import numpy as np
import matplotlib as mpl
from scipy import interpolate
import matplotlib.pyplot as plt
from matplotlib import gridspec
mpl.rcParams.update({'font.size': 16})
from netCDF4 import Dataset
import pickle
import numpy.ma as ma
import os
import os.path as op
import re
import f90nml
from mpl_toolkits.axes_grid1 import make_axes_locatable
import copy

from importlib import reload


class OIBObs():
    """
    Operation Ice Bridge processed isochrones for Greenland
    """
    def __init__(self, resolution, yelmoxpath, obspath, version='v2'):
    # Load surface and bed topographies:
        if resolution == 8:
            path = op.join(yelmoxpath, 'ice_data/Greenland/GRL-8KM/GRL-8KM_TOPO-M17.nc')
            self.dx = 8
        elif resolution   == 16:
            path = op.join(yelmoxpath, 'ice_data/Greenland/GRL-16KM/GRL-16KM_TOPO-M17.nc')
            self.dx = 16
        else:
            print('Observations do not exist at resolution ' + str(resolution) + '.')
            return
        ncfile_topo = Dataset(path, 'r')
        self.srf = ncfile_topo.variables['z_srf'][:].copy().astype(float)
        self.bed = ncfile_topo.variables['z_bed'][:].copy().astype(float)
        ncfile_topo.close()

        self.bed[self.bed==0.0] = np.nan
        self.srf[self.srf==0.0] = self.bed[self.srf==0.0]
        self.ice = self.srf - self.bed
        self.ice_mask = np.zeros_like(self.ice)
        self.ice_mask = (np.where(self.ice<1,1,0))
        self.mask_1km = np.zeros_like(self.ice)
        self.mask_1km = (np.where(self.ice<1000.,1,0))
        self.mask_1p5km = np.zeros_like(self.ice)
        self.mask_1p5km = (np.where(self.ice<1500.,1,0))
        self.mask_2km = np.zeros_like(self.ice)
        self.mask_2km = (np.where(self.ice<2000.,1,0))
        self.coastline = self.srf.copy()

        if version == 'v1':
            # Load age of isochrones:
            ncfile_nasa = Dataset(op.join(obspath, 'Greenland_age_grid.nc'), 'r')
            self.age_iso_nasa = ncfile_nasa.variables['age_iso'][:].copy()
            self.age_iso_ce = 2000. - ncfile_nasa.variables['age_iso'][:].copy()
            ncfile_nasa.close()

            # Load isochrone depths:
            if resolution  == 16:
                nasa_file = open(op.join(obspath, 'interp_icebridge_yelmo_16km.bin'), 'rb')
                data = pickle.load(nasa_file, encoding="latin1")
                nasa_file.close()

                self.thickness = data[2]
                self.age_norm = data[3]
                self.age_norm_uncert = data[4]
                self.depth_iso = data[5]
                self.depth_iso_uncert = data[6]

                self.depth_iso[self.depth_iso==0] = np.nan
                self.depth_iso_uncert[self.depth_iso_uncert==0] = np.nan
                self.dsum_iso = np.tile(self.thickness,[4,1,1]) - self.depth_iso

                #self.coastline[self.srf>0.] = 2.
                self.coastline[:,50:53]     = np.nan
                self.coastline[:,0:3]       = np.nan
                self.coastline[75:-1,0:10]  = np.nan
                self.coastline[82:-1,10:16] = np.nan
                self.coastline[80:82,10:13] = np.nan

            # NOTE different shape from 16km files
            elif resolution == 8:
                data = Dataset(op.join(obspath, 'interp_icebridge_yelmo_8km.nc'), 'r')

                self.thickness = np.transpose(data.variables['thickness'][:].copy())
                self.age_norm = np.transpose(data.variables['age_norm'][:].copy())
                self.age_norm_uncert = np.transpose(data.variables['age_norm_uncert'][:].copy())
                self.depth_iso = np.transpose(data.variables['depth_iso'][:].copy())
                self.depth_iso_uncert = np.transpose(data.variables['depth_iso_uncert'][:].copy())

                self.depth_iso[self.depth_iso==0] = np.nan
                self.depth_iso_uncert[self.depth_iso_uncert==0] = np.nan
                self.dsum_iso = np.tile(self.thickness,[4,1,1]) - self.depth_iso

            else:
                print('Observations do not exist at resolution ' + str(resolution) + '.')
                return

        elif version == 'v2':
            try:
                fn = 'interp_icebridge_{}km_v2.nc'.format(resolution)
                data = Dataset(op.join(obspath, fn), 'r')
                self.age_iso_nasa = data.variables['age'][:].copy()
                self.age_iso_ce = 2000. - 1000*data.variables['age'][:].copy()
                self.depth_iso = data.variables['depth'][:].copy()
                self.depth_iso_std = data.variables['std'][:].copy()
                self.thickness = self.srf - self.bed
                self.dsum_iso = np.tile(self.thickness,[10,1,1]) - self.depth_iso

            except FileNotFoundError:
                print('Interpolated OIB data does not exist at resolution ' + str(resolution) + ', create first.')
                return


        # Ice core coordinates (lat,lon):
        self.summit_coords = [72.58,-38.46]
        self.neem_coords = [79.0,-50.0]
        self.dye3_coords = [65.0,-43.75]
        self.ngrip_coords = [75.16,-42.5]
        self.egrip_coords = [75.5,-36]



class Elsa0():
    """
    ELSA object for ELSA-offline
    Reads ELSA parameter settings and data (layer thickness, cumulative layer thickness, total ice thickness, horizontal velocities)
    """
    def __init__(self, output_dir, nml_name):
        self.nml_name = nml_name
        self.path = output_dir
        nml_file = op.join(self.path, self.nml_name)
        try:
            self.config = f90nml.read(nml_file)
        except FileNotFoundError:
            errmsg = '{} does not exist'.format(nml_file)
            print(errmsg)
            raise FileNotFoundError

        self.exp_name = self.config['elsa', 'exp_name']
        self.n_layers_init = self.config['elsa','n_layers_init']
        self.update_factor = self.config['elsa','update_factor']
        self.grid_factor = int(self.config['elsa', 'grid_factor'])
        self.elsa_out = self.config['elsa','elsa_out']
        self.dye_tracer = self.config['elsa','use_dye_tracer']
        self.allow_pos_bmb = self.config['elsa','allow_pos_bmb']
        if self.config['elsa','layer_resolution'] == 0:
            self.layer_resolution = np.nan
            self.layer_file = op.split(self.config['elsa','layer_file'])[-1]
        elif op.split(self.config['elsa','layer_file'])[-1] == 'NA':
            self.layer_resolution = self.config['elsa','layer_resolution']
            self.layer_file = np.nan

        self.model_timestep = self.config['model', 'timestep']
        self.time_init = self.config['model', 'time_init']
        self.time_end = self.config['model', 'time_end']
        self.runtime = self.time_end-self.time_init
        self.dx = self.config['model', 'res_x']
        self.dy = self.config['model', 'res_y']
        input_netcdf = self.config['NetCDF', 'NetCDF_in']
        self.input_netcdf = re.sub('{exp_name}', self.exp_name, input_netcdf)

        # compute elsa's temporal resolution
        self.timestep = self.update_factor * self.model_timestep
        # compute total layer number in elsa output files
        if self.layer_file is np.nan:
            self.kmax = int((self.runtime/self.layer_resolution) + self.n_layers_init) # maximum number of layers
            self.isochrones = np.ma.fix_invalid(np.hstack((np.empty((self.n_layers_init))*np.nan, np.arange(self.time_init, self.time_end, self.layer_resolution))))
        else:
            content = np.loadtxt(op.join(self.path, self.layer_file))
            self.kmax = int(np.size(content) + 1 + self.n_layers_init)
            self.isochrones = np.ma.fix_invalid(np.hstack((np.empty((self.n_layers_init))*np.nan, np.sort(content), self.time_end)))


    def load_npy(self, varname):
        """
        loads data from isochrones output (.npy files)
        varname: variable name: diso, dsum, hice, smb, vx, vy
        """
        filename = op.join(self.path, varname + '.npy')
        var = np.load(filename)

        if len(var.shape) == 2:
            return var
        elif len(var.shape) == 3:
            return np.rollaxis(np.flipud(var), 2, 0)
        else:
            errmsg = 'Implement correct reshaping for 4d variables'
            raise Exception(errmsg)
        # return np.transpose(var)


    def get_data_on_isochrone(self, varn, isochrone):
        """
        find data for given isochrone. Input:
        varn: variable name
        isochrone: age (CE)
        """
        k = self.get_isochrone_index(isochrone)
        var = self.load_npy(varn)
        return var[k]


    def get_isochrone_depth_below_surface(self, isochrone):
        """
        find depth below surface for given isochrone. Input:
        varn: variable name
        isochrone: age (CE)
        """
        k = self.get_isochrone_index(isochrone)
        dsum = self.load_npy('dsum')
        var = dsum[-1] - dsum[k]
        return var


    def get_isochrone_index(self, isochrone, get_diff=False):
        """
        input: isochrone age (CE) for wanted isochrone
        returns the index (in the vertical) for the wanted isochrone
        """
        k = np.ma.argmin(np.abs(self.isochrones-isochrone))
        diff = (isochrone-self.isochrones)[k]

        if get_diff is True:
            return k, diff
        else:
            return k


    def ce_to_yor(self, time):
        """
        computes year-of-run from Common Time
        """
        return (time - self.time_init)


    def yor_to_ce(self, time):
        """
        computes year-of-run from Common Time
        """
        return (time + self.time_init)


    def load_nc(self, varname, get_time= False, get_unit= False):
        """
        loads yelmo 2D ncdf
        see file for all variable names
        """
        ncfile = Dataset(op.join(self.path, self.exp_name+'.2D.nc'), 'r')
        var = ncfile.variables[varname][:].copy()
        infos = {}

        if get_time is True:
            time_ = ncfile.variables['time'][:].copy()
            infos['time'] = time_

        if get_unit is True:
            unit = ncfile.variables[varname].units
            lname = ncfile.variables[varname].long_name
            infos['unit'] = unit
            infos['long_name'] = lname

        ncfile.close()
        if not infos:
            return var
        else:
            return var, infos


    def get_bed_from_netcdf(self, ce=None):
        """
        return coastline as a function of (time, lat, lon) from the simulation.
        """
        zbed, info = self.load_nc('z_bed', get_time=True)
        try:
            idx = np.argmin(np.abs(info['time']-ce))
            return zbed, idx
        except TypeError:
            return zbed


    def get_ice_mask_from_netcdf(self, ce=None):
        """
        return ice mask as a function of (time, lat, lon) from the simulation.
        """
        ice, info = self.load_nc('H_ice', get_time=True)
        idx = np.argmin(np.abs(info['time']-ce))
        ice_mask = np.ma.masked_where(ice==0, ice)
        try:
            return ice_mask.mask, idx
        except TypeError:
            return ice_mask.mask


    def get_ice_mask(self):
        """
        return ice mask for all isochrones at end of simulation(lat, lon)
        """
        ice = self.load_npy('dsum')
        ice_mask = np.ma.masked_where(ice==0, ice)

        return ice_mask.mask


    def idx_latlon(self, coords):
        """
        find node in yelmo output closest to given location. Input:
        (lat, lon)
        """
        self.lat = self.load_nc('lat2D')
        self.lon = self.load_nc('lon2D')+360

        lat, lon = coords
        if (lon < 0.):
            lon = lon + 360.
        # find closest node in yelmo:
        dist = ((self.lat-lat)**2+(self.lon-lon)**2)**.5
        if (np.min(dist) < 1.):
            node = np.unravel_index(np.argmin(dist), dist.shape)
        else:
            print('yelmo_utils::indx_latlon: Did not find nearest node.')
            node = np.nan

        return node


    def does_run_exist(self):
        """
        checks if path exists
        """
        return os.path.isdir(self.path)



class ElsaYelmo(Elsa0):
    """
    ELSA object for coupled ELSA-yelmo models
    Reads ELSA parameter settings and data (layer thickness, cumulative layer thickness, total ice thickness, horizontal velocities)
    Reads yelmo data
    """
    def __init__(self, output_dir, nml_name):
        self.nml_name = nml_name
        self.path = output_dir
        nml_file = op.join(self.path, self.nml_name)
        try:
            self.config = f90nml.read(nml_file)
        except FileNotFoundError:
            errmsg = '{} does not exist'.format(nml_file)
            print(errmsg)
            raise FileNotFoundError

        self.n_layers_init = self.config['elsa','n_layers_init']
        self.update_factor = self.config['elsa','update_factor']
        self.grid_factor = int(self.config['elsa', 'grid_factor'])
        self.elsa_out = self.config['elsa','elsa_out']
        self.dye_tracer = self.config['elsa','use_dye_tracer']
        self.allow_pos_bmb = self.config['elsa','allow_pos_bmb']
        if self.config['elsa','layer_resolution'] == 0:
            self.layer_resolution = np.nan
            self.layer_file = op.split(self.config['elsa','layer_file'])[-1]
        elif op.split(self.config['elsa','layer_file'])[-1] == 'NA':
            self.layer_resolution = self.config['elsa','layer_resolution']
            self.layer_file = np.nan

        self.grid_name = self.config['yelmo','grid_name']
        self.exp_name = self.config['ctrl', 'exp_name']
        self.time_init = self.config['ctrl','time_init']
        self.time_end = self.config['ctrl','time_end']
        self.runtime = self.time_end-self.time_init
        self.model_timestep = self.config['ctrl','dtt']

        gridinfo_filename = op.join(self.path, 'ice_data/Greenland', self.grid_name, self.grid_name+'_REGIONS.nc')
        f = Dataset(gridinfo_filename, 'r')
        self.lon = f.variables['lon2D'][:].copy()+360
        self.lat = f.variables['lat2D'][:].copy()
        self.area = f.variables['area'][:].copy()
        self.imax = f.dimensions['xc'].size
        self.jmax = f.dimensions['yc'].size
        f.close()
        self.dx = int(str.split(self.grid_name, '-')[1].strip('KM'))
        self.dy = int(str.split(self.grid_name, '-')[1].strip('KM'))

        # compute elsa's temporal resolution
        self.timestep = self.update_factor * self.model_timestep
        # compute total layer number in elsa output files
        if self.layer_file is np.nan:
            self.kmax = int((self.runtime/self.layer_resolution) + self.n_layers_init) # maximum number of layers
            self.isochrones = np.ma.fix_invalid(np.hstack((np.empty((self.n_layers_init))*np.nan, np.arange(self.time_init, self.time_end, self.layer_resolution))))
        else:
            content = np.loadtxt(op.join(self.path, self.layer_file))
            self.isochrones = np.ma.fix_invalid(np.hstack((np.empty((self.n_layers_init))*np.nan, np.sort(content), self.time_end)))
            self.kmax = int(np.size(content) + 1 + self.n_layers_init)

        return


    def load_npy(self, varname):
        """
        loads data from isochrones output (.npy files)
        varname: variable name
        """
        filename = op.join(self.path, '.'.join([self.exp_name, varname, 'npy']))
        var = np.load(filename)
        return np.transpose(var)
    # NOTE check if dimension is correct here



