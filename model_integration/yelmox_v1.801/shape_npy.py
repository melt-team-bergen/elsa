"""
Description: 

Author: Therese Rieckh | therese.rieckh@uib.no

"""
import argparse
import numpy as np
import os
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument('--path', type=str)
args = parser.parse_args()
files = os.listdir(Path(args.path))

for x in [f for f in files if '.npy' in f]:
    fn = os.path.join(Path(args.path),x)
    var = np.load(fn)

    if var.ndim == 2:
        var = np.flipud(var.T)
        np.save(fn, var)
        print('ndim 2, ', var.shape)

    if var.ndim == 3:
        var = np.fliplr(np.transpose(var, axes=(2,1,0)))
        np.save(fn, var)
        print('ndim 3, ', var.shape)




