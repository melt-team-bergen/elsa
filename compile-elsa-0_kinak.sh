#ELSA-0 compilation script
#MELT team
#University of Bergen, 2023

echo "Compiling elsa-0"
cd src

FC=gfortran #the compiler to use, only tested for gfortran

NCF=-I/usr/include #the location of netcdf.mod with the '-I' flag for 'include'. This can be installed with 'apt-get install libnetcdff-dev'. 
libNCF="-lnetcdff -lnetcdf"
#NETCDF_DIR still needs to point to the correct path (e.g. export NETCDF_DIR=/lib/x86_64-linux-gnu/libnetcdf.so in your .bashrc)

lis=-I/usr/local/bin/include #location of the lis library header lisf.h. Follow the docs carefully and make sure to use --enable-f90 and --enable-fortran flags.
liblis="-L/usr/local/bin/lib -llis"

mpi=-I/home/kinak/Documents/svn/ISSM/trunk/externalpackages/petsc/install/include #location of mpi

$FC -cpp -g \
        elsa_classes.f90 npy.f90 nml.f90 load_netcdf.f90 f90getopt.F90 elsa_isochrones.f90 \
	elsa0.f90 -o elsa0 $NCF $lis $mpi $liblis $libNCF #-g flag can be included for debugging but is not essential

if [ -f ./elsa0 ]; then
  echo "elsa-0 compiled, tidying"
else
  echo " " #something went wrong but you probably have an error for that already
fi

mv elsa0 ../elsa0
rm *.mod
cd ../

